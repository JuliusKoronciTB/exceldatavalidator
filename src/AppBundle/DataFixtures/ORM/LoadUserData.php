<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 7/8/16
 * Time: 9:58 AM
 */

namespace AdminBundle\DataFixtures\ORM;


use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUserData
 *
 * @package AdminBundle\DataFixtures\ORM
 */
class LoadUserData implements FixtureInterface , ContainerAwareInterface , OrderedFixtureInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setEmail('koronci@trimbroker.com')
             ->setShortName('JK')
             ->setName('Július')
             ->setSurname('Koronci')
             ->setCompany('TRIM Broker');
        $plainPassword = 'Abs77$';
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($user , $plainPassword);
        $user->setPassword($encoded);
        $manager->persist($user);

        $manager->flush();
    }

    /**
     * Sets the container.
     *
     * @param ContainerInterface|null $container A ContainerInterface instance or null
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * Get the order of this fixture
     *
     * @return integer
     */
    public function getOrder()
    {
        return 1;
    }
}