<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 *
 * @ORM\Table(name="report")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReportRepository")
 */
class Report
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sheet_name", type="string", length=255)
     */
    private $sheetName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="reports")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="QuestionCategory", mappedBy="report", cascade={"persist", "remove"})
     */
    private $questionCategories;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="ExcelData", mappedBy="report", cascade={"persist", "remove"})
     */
    private $excelData;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="QuestionCategoryColumn", mappedBy="report", cascade={"persist", "remove"})
     */
    private $questionCategoryColumns;

    public function __construct()
    {
        $this->questionCategories = new ArrayCollection();
        $this->questionCategoryColumns = new ArrayCollection();
        $this->excelData = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Report
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sheetName
     *
     * @param string $sheetName
     *
     * @return Report
     */
    public function setSheetName($sheetName)
    {
        $this->sheetName = $sheetName;

        return $this;
    }

    /**
     * Get sheetName
     *
     * @return string
     */
    public function getSheetName()
    {
        return $this->sheetName;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Report
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Report
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add questionCategory
     *
     * @param QuestionCategory $questionCategory
     *
     * @return Report
     */
    public function addQuestionCategory(QuestionCategory $questionCategory)
    {
        $this->questionCategories[] = $questionCategory;

        return $this;
    }

    /**
     * Remove questionCategory
     *
     * @param QuestionCategory $questionCategory
     */
    public function removeQuestionCategory(QuestionCategory $questionCategory)
    {
        $this->questionCategories->removeElement($questionCategory);
    }

    /**
     * Get questionCategories
     *
     * @return Collection
     */
    public function getQuestionCategories()
    {
        return $this->questionCategories;
    }

    /**
     * Add questionCategoryColumn
     *
     * @param QuestionCategoryColumn $questionCategoryColumn
     *
     * @return Report
     */
    public function addQuestionCategoryColumn(QuestionCategoryColumn $questionCategoryColumn)
    {
        $this->questionCategoryColumns[] = $questionCategoryColumn;

        return $this;
    }

    /**
     * Remove questionCategoryColumn
     *
     * @param QuestionCategoryColumn $questionCategoryColumn
     */
    public function removeQuestionCategoryColumn(QuestionCategoryColumn $questionCategoryColumn)
    {
        $this->questionCategoryColumns->removeElement($questionCategoryColumn);
    }

    /**
     * Get questionCategoryColumns
     *
     * @return Collection
     */
    public function getQuestionCategoryColumns()
    {
        return $this->questionCategoryColumns;
    }

    /**
     * Add excelDatum
     *
     * @param \AppBundle\Entity\ExcelData $excelDatum
     *
     * @return Report
     */
    public function addExcelDatum(\AppBundle\Entity\ExcelData $excelDatum)
    {
        $this->excelData[] = $excelDatum;

        return $this;
    }

    /**
     * Remove excelDatum
     *
     * @param \AppBundle\Entity\ExcelData $excelDatum
     */
    public function removeExcelDatum(\AppBundle\Entity\ExcelData $excelDatum)
    {
        $this->excelData->removeElement($excelDatum);
    }

    /**
     * Get excelData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExcelData()
    {
        return $this->excelData;
    }
}
