<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * AdvancedValidation
 *
 * @ORM\Table(name="advanced_validation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdvancedValidationRepository")
 */
class AdvancedValidation
{
    const LOGIC_AND = 'and';
    const LOGIC_OR = 'or';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    /**
     * @var string
     *
     * @ORM\Column(name="logic", type="string", length=10)
     */
    private $logic;

    /**
     * @var string
     *
     * @ORM\Column(name="display_code", type="string", length=255)
     */
    private $displayCode;

    /**
     * @var string
     *
     * @ORM\Column(name="display_reason", type="string", length=255)
     */
    private $displayReason;

    /**
     * @var string
     *
     * @ORM\Column(name="display_message", type="text")
     */
    private $displayMessage;

    /**
     * Category
     *
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="advancedValidations")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\SimpleValidation", inversedBy="advancedValidations")
     * @ORM\JoinTable(name="advanced_validation_has_simple_validation")
     */
    private $simpleValidations;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="AdvancedValidation", mappedBy="childrenAdvancedValidations")
     *
     */
    private $parentAdvancedValidations;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="AdvancedValidation", inversedBy="parentAdvancedValidations")
     * @ORM\JoinTable(name="advanced_validation_has_advanced_validation")
     */
    private $childrenAdvancedValidations;

    public function __construct()
    {
        $this->simpleValidations = new ArrayCollection();
        $this->parentAdvancedValidations = new  ArrayCollection();
        $this->childrenAdvancedValidations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return AdvancedValidation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return AdvancedValidation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set logic
     *
     * @param string $logic
     *
     * @return AdvancedValidation
     */
    public function setLogic($logic)
    {
        $this->logic = $logic;

        return $this;
    }

    /**
     * Get logic
     *
     * @return string
     */
    public function getLogic()
    {
        return $this->logic;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return AdvancedValidation
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add simpleValidation
     *
     * @param SimpleValidation $simpleValidation
     *
     * @return AdvancedValidation
     */
    public function addSimpleValidation(SimpleValidation $simpleValidation)
    {
        $simpleValidation->addAdvancedValidation($this);
        $this->simpleValidations[] = $simpleValidation;

        return $this;
    }

    /**
     * Remove simpleValidation
     *
     * @param SimpleValidation $simpleValidation
     */
    public function removeSimpleValidation(SimpleValidation $simpleValidation)
    {
        $this->simpleValidations->removeElement($simpleValidation);
    }

    /**
     * Get simpleValidations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSimpleValidations()
    {
        return $this->simpleValidations;
    }

    /**
     * Add parentAdvancedValidation
     *
     * @param AdvancedValidation $parentAdvancedValidation
     *
     * @return AdvancedValidation
     */
    public function addParentAdvancedValidation(AdvancedValidation $parentAdvancedValidation)
    {
        $parentAdvancedValidation->addChildrenAdvancedValidation($this);
        $this->parentAdvancedValidations[] = $parentAdvancedValidation;

        return $this;
    }

    /**
     * Remove parentAdvancedValidation
     *
     * @param AdvancedValidation $parentAdvancedValidation
     */
    public function removeParentAdvancedValidation(AdvancedValidation $parentAdvancedValidation)
    {
        $this->parentAdvancedValidations->removeElement($parentAdvancedValidation);
    }

    /**
     * Get parentAdvancedValidations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParentAdvancedValidations()
    {
        return $this->parentAdvancedValidations;
    }

    /**
     * Add childrenAdvancedValidation
     *
     * @param AdvancedValidation $childrenAdvancedValidation
     *
     * @return AdvancedValidation
     */
    public function addChildrenAdvancedValidation(AdvancedValidation $childrenAdvancedValidation)
    {
        $childrenAdvancedValidation->addParentAdvancedValidation($this);
        $this->childrenAdvancedValidations[] = $childrenAdvancedValidation;

        return $this;
    }

    /**
     * Remove childrenAdvancedValidation
     *
     * @param AdvancedValidation $childrenAdvancedValidation
     */
    public function removeChildrenAdvancedValidation(AdvancedValidation $childrenAdvancedValidation)
    {
        $this->childrenAdvancedValidations->removeElement($childrenAdvancedValidation);
    }

    /**
     * Get childrenAdvancedValidations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildrenAdvancedValidations()
    {
        return $this->childrenAdvancedValidations;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }


    /**
     * Set displayCode
     *
     * @param string $displayCode
     *
     * @return AdvancedValidation
     */
    public function setDisplayCode($displayCode)
    {
        $this->displayCode = $displayCode;

        return $this;
    }

    /**
     * Get displayCode
     *
     * @return string
     */
    public function getDisplayCode()
    {
        return $this->displayCode;
    }

    /**
     * Set displayReason
     *
     * @param string $displayReason
     *
     * @return AdvancedValidation
     */
    public function setDisplayReason($displayReason)
    {
        $this->displayReason = $displayReason;

        return $this;
    }

    /**
     * Get displayReason
     *
     * @return string
     */
    public function getDisplayReason()
    {
        return $this->displayReason;
    }

    /**
     * Set displayMessage
     *
     * @param string $displayMessage
     *
     * @return AdvancedValidation
     */
    public function setDisplayMessage($displayMessage)
    {
        $this->displayMessage = $displayMessage;

        return $this;
    }

    /**
     * Get displayMessage
     *
     * @return string
     */
    public function getDisplayMessage()
    {
        return $this->displayMessage;
    }
}
