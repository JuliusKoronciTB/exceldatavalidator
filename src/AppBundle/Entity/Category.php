<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;


/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    /**
     * @var string
     *
     * @ORM\Column(name="external_list", type="boolean", options={"default"=0})
     */
    private $externalList = false;

    /**
     *
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="Excel", mappedBy="category", cascade={"persist", "remove"})
     */
    private $excels;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="SimpleValidation", mappedBy="category", cascade={"persist", "remove"})
     */
    private $simpleValidations;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="AdvancedValidation", mappedBy="category", cascade={"persist", "remove"})
     */
    private $advancedValidations;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="Report", mappedBy="category",cascade={"persist", "remove"})
     */
    private $reports;

    public function __construct()
    {
        $this->excels = new ArrayCollection();
        $this->reports = new ArrayCollection();
        $this->simpleValidations = new ArrayCollection();
        $this->advancedValidations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Category
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add excel
     *
     * @param Excel $excel
     *
     * @return Category
     */
    public function addExcel(Excel $excel)
    {
        $this->excels[] = $excel;

        return $this;
    }

    /**
     * Remove excel
     *
     * @param Excel $excel
     */
    public function removeExcel(Excel $excel)
    {
        $this->excels->removeElement($excel);
    }

    /**
     * Get excels
     *
     * @return Collection
     */
    public function getExcels()
    {
        return $this->excels;
    }

    /**
     * Add report
     *
     * @param Report $report
     *
     * @return Category
     */
    public function addReport(Report $report)
    {
        $this->reports[] = $report;

        return $this;
    }

    /**
     * Remove report
     *
     * @param Report $report
     */
    public function removeReport(Report $report)
    {
        $this->reports->removeElement($report);
    }

    /**
     * Get reports
     *
     * @return Collection
     */
    public function getReports()
    {
        return $this->reports;
    }

    /**
     * Add simpleValidation
     *
     * @param SimpleValidation $simpleValidation
     *
     * @return Category
     */
    public function addSimpleValidation(SimpleValidation $simpleValidation)
    {
        $this->simpleValidations[] = $simpleValidation;

        return $this;
    }

    /**
     * Remove simpleValidation
     *
     * @param SimpleValidation $simpleValidation
     */
    public function removeSimpleValidation(SimpleValidation $simpleValidation)
    {
        $this->simpleValidations->removeElement($simpleValidation);
    }

    /**
     * Get simpleValidations
     *
     * @return Collection
     */
    public function getSimpleValidations()
    {
        return $this->simpleValidations;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add advancedValidation
     *
     * @param AdvancedValidation $advancedValidation
     *
     * @return Category
     */
    public function addAdvancedValidation(AdvancedValidation $advancedValidation)
    {
        $this->advancedValidations[] = $advancedValidation;

        return $this;
    }

    /**
     * Remove advancedValidation
     *
     * @param AdvancedValidation $advancedValidation
     */
    public function removeAdvancedValidation(AdvancedValidation $advancedValidation)
    {
        $this->advancedValidations->removeElement($advancedValidation);
    }

    /**
     * Get advancedValidations
     *
     * @return Collection
     */
    public function getAdvancedValidations()
    {
        return $this->advancedValidations;
    }

    /**
     * Set externalList
     *
     * @param boolean $externalList
     *
     * @return Category
     */
    public function setExternalList($externalList)
    {
        $this->externalList = $externalList;

        return $this;
    }

    /**
     * Get externalList
     *
     * @return boolean
     */
    public function getExternalList()
    {
        return $this->externalList;
    }
}
