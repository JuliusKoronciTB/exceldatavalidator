<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * Excel
 *
 * @ORM\Table(name="excel")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExcelRepository")
 */
class Excel
{
    use TimestampableEntity;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="processed", type="boolean", options={"default"=0})
     */
    private $processed = false;
    /**
     * @var string
     *
     * @ORM\Column(name="valid", type="boolean", options={"default"=0})
     */
    private $valid = false;

    /**
     * @var string
     *
     * @ORM\Column(name="validated", type="boolean", options={"default"=0})
     */
    private $validated = false;

    /**
     * @var string
     *
     * @ORM\Column(name="report_log", type="text", nullable=true)
     */
    private $ReportLog;
    /**
     * Creator
     * @ORM\OneToMany(targetEntity="ExcelData", mappedBy="excel", cascade={"persist", "remove"})
     */
    private $excelData;

    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="User", inversedBy="excels")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="excels")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->excelData = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return Excel
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Excel
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Excel
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return Excel
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add excelDatum
     *
     * @param ExcelData $excelDatum
     *
     * @return Excel
     */
    public function addExcelDatum(ExcelData $excelDatum)
    {
        $this->excelData[] = $excelDatum;

        return $this;
    }

    /**
     * Remove excelDatum
     *
     * @param ExcelData $excelDatum
     */
    public function removeExcelDatum(ExcelData $excelDatum)
    {
        $this->excelData->removeElement($excelDatum);
    }

    /**
     * Get excelData
     *
     * @return Collection
     */
    public function getExcelData()
    {
        return $this->excelData;
    }

    /**
     * Set processed
     *
     * @param boolean $processed
     *
     * @return Excel
     */
    public function setProcessed($processed)
    {
        $this->processed = $processed;

        return $this;
    }

    /**
     * Get processed
     *
     * @return boolean
     */
    public function getProcessed()
    {
        return $this->processed;
    }

    /**
     * Set valid
     *
     * @param boolean $valid
     *
     * @return Excel
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * Get valid
     *
     * @return boolean
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     *
     * @return Excel
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set reportLog
     *
     * @param string $reportLog
     *
     * @return Excel
     */
    public function setReportLog($reportLog)
    {
        $this->ReportLog = serialize($reportLog);

        return $this;
    }

    /**
     * Get reportLog
     *
     * @return string
     */
    public function getReportLog()
    {
        return unserialize($this->ReportLog);
    }
}
