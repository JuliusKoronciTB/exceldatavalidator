<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SimpleValidation
 *
 * @ORM\Table(name="simple_validation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SimpleValidationRepository")
 */
class SimpleValidation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var bool
     *
     * @ORM\Column(name="auto_run", type="boolean", options={"default"=false})
     */
    private $autoRun = false;

    /**
     * @var string
     *
     * @ORM\Column(name="source_field", type="string", length=255, nullable=true)
     */
    private $sourceField;

    /**
     * @var string
     *
     * @ORM\Column(name="source_field2", type="string", length=255, nullable=true)
     */
    private $sourceField2;

    /**
     * @var string
     *
     * @ORM\Column(name="source_sheet", type="string", length=255, nullable=true)
     */
    private $sourceSheet;

    /**
     * @var string
     *
     * @ORM\Column(name="source_list", type="string", length=255, nullable=true)
     */
    private $sourceList;

    /**
     * @var string
     *
     * @ORM\Column(name="compare_to", type="string", length=255, nullable=true)
     */
    private $compareTo;

    /**
     * @var string
     *
     * @ORM\Column(name="compare_to2", type="string", length=255, nullable=true)
     */
    private $compareTo2;
    /**
     * @var string
     *
     * @ORM\Column(name="compare_to_sheet", type="string", length=255, nullable=true)
     */
    private $compareToSheet;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="display_code", type="string", length=255)
     */
    private $displayCode;

    /**
     * @var string
     *
     * @ORM\Column(name="display_reason", type="string", length=255)
     */
    private $displayReason;

    /**
     * @var string
     *
     * @ORM\Column(name="display_message", type="text",  nullable=true)
     */
    private $displayMessage;


    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="simpleValidations")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\QuestionCategory", inversedBy="externalListsValidations")
     * @ORM\JoinColumn(name="external_list_id", referencedColumnName="id", nullable=true)
     */
    private $externalList;

    /**
     * @var
     *
     * @ORM\ManyToMany(targetEntity="AdvancedValidation", mappedBy="simpleValidations")
     */
    private $advancedValidations;


    public function __construct()
    {
        $this->advancedValidations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SimpleValidation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SimpleValidation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set autoRun
     *
     * @param boolean $autoRun
     *
     * @return SimpleValidation
     */
    public function setAutoRun($autoRun)
    {
        $this->autoRun = $autoRun;

        return $this;
    }

    /**
     * Get autoRun
     *
     * @return boolean
     */
    public function getAutoRun()
    {
        return $this->autoRun;
    }

    /**
     * Set sourceField
     *
     * @param string $sourceField
     *
     * @return SimpleValidation
     */
    public function setSourceField($sourceField)
    {
        $this->sourceField = $sourceField;

        return $this;
    }

    /**
     * Get sourceField
     *
     * @return string
     */
    public function getSourceField()
    {
        return $this->sourceField;
    }

    /**
     * Set compareTo
     *
     * @param string $compareTo
     *
     * @return SimpleValidation
     */
    public function setCompareTo($compareTo)
    {
        $this->compareTo = $compareTo;

        return $this;
    }

    /**
     * Get compareTo
     *
     * @return string
     */
    public function getCompareTo()
    {
        return $this->compareTo;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return SimpleValidation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set displayCode
     *
     * @param string $displayCode
     *
     * @return SimpleValidation
     */
    public function setDisplayCode($displayCode)
    {
        $this->displayCode = $displayCode;

        return $this;
    }

    /**
     * Get displayCode
     *
     * @return string
     */
    public function getDisplayCode()
    {
        return $this->displayCode;
    }

    /**
     * Set displayReason
     *
     * @param string $displayReason
     *
     * @return SimpleValidation
     */
    public function setDisplayReason($displayReason)
    {
        $this->displayReason = $displayReason;

        return $this;
    }

    /**
     * Get displayReason
     *
     * @return string
     */
    public function getDisplayReason()
    {
        return $this->displayReason;
    }

    /**
     * Set displayMessage
     *
     * @param string $displayMessage
     *
     * @return SimpleValidation
     */
    public function setDisplayMessage($displayMessage)
    {
        $this->displayMessage = $displayMessage;

        return $this;
    }

    /**
     * Get displayMessage
     *
     * @return string
     */
    public function getDisplayMessage()
    {
        return $this->displayMessage;
    }

    /**
     * Set category
     *
     * @param Category $category
     *
     * @return SimpleValidation
     */
    public function setCategory(Category $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set sourceSheet
     *
     * @param string $sourceSheet
     *
     * @return SimpleValidation
     */
    public function setSourceSheet($sourceSheet)
    {
        $this->sourceSheet = $sourceSheet;

        return $this;
    }

    /**
     * Get sourceSheet
     *
     * @return string
     */
    public function getSourceSheet()
    {
        return $this->sourceSheet;
    }

    /**
     * Set compareToSheet
     *
     * @param string $compareToSheet
     *
     * @return SimpleValidation
     */
    public function setCompareToSheet($compareToSheet)
    {
        $this->compareToSheet = $compareToSheet;

        return $this;
    }

    /**
     * Get compareToSheet
     *
     * @return string
     */
    public function getCompareToSheet()
    {
        return $this->compareToSheet;
    }

    /**
     * Add advancedValidation
     *
     * @param AdvancedValidation $advancedValidation
     *
     * @return SimpleValidation
     */
    public function addAdvancedValidation(AdvancedValidation $advancedValidation)
    {
        $this->advancedValidations[] = $advancedValidation;

        return $this;
    }

    /**
     * Remove advancedValidation
     *
     * @param AdvancedValidation $advancedValidation
     */
    public function removeAdvancedValidation(AdvancedValidation $advancedValidation)
    {
        $this->advancedValidations->removeElement($advancedValidation);
    }

    /**
     * Get advancedValidations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdvancedValidations()
    {
        return $this->advancedValidations;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }


    /**
     * Set sourceList
     *
     * @param string $sourceList
     *
     * @return SimpleValidation
     */
    public function setSourceList($sourceList)
    {
        $this->sourceList = $sourceList;

        return $this;
    }

    /**
     * Get sourceList
     *
     * @return string
     */
    public function getSourceList()
    {
        return $this->sourceList;
    }

    /**
     * Set externalList
     *
     * @param QuestionCategory $externalList
     *
     * @return SimpleValidation
     */
    public function setExternalList(QuestionCategory $externalList = null)
    {
        $this->externalList = $externalList;

        return $this;
    }

    /**
     * Get externalList
     *
     * @return QuestionCategory
     */
    public function getExternalList()
    {
        return $this->externalList;
    }

    /**
     * Set sourceField2
     *
     * @param string $sourceField2
     *
     * @return SimpleValidation
     */
    public function setSourceField2($sourceField2)
    {
        $this->sourceField2 = $sourceField2;

        return $this;
    }

    /**
     * Get sourceField2
     *
     * @return string
     */
    public function getSourceField2()
    {
        return $this->sourceField2;
    }

    /**
     * Set compareTo2
     *
     * @param string $compareTo2
     *
     * @return SimpleValidation
     */
    public function setCompareTo2($compareTo2)
    {
        $this->compareTo2 = $compareTo2;

        return $this;
    }

    /**
     * Get compareTo2
     *
     * @return string
     */
    public function getCompareTo2()
    {
        return $this->compareTo2;
    }
}
