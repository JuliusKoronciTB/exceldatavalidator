<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExcelData
 *
 * @ORM\Table(name="excel_data",indexes={
 *     @ORM\Index(name="search_idx", columns={"col", "row"}),
 *     @ORM\Index(name="search_idx_col", columns={"col"}),
 *     @ORM\Index(name="search_idx_row", columns={"row"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExcelDataRepository")
 */
class ExcelData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="row", type="integer")
     */
    private $row;

    /**
     * @var string
     *
     * @ORM\Column(name="col", type="string", length=15)
     */
    private $col;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_value", type="datetime", nullable=true)
     */
    private $dateValue;

    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="Excel", inversedBy="excelData")
     * @ORM\JoinColumn(name="excel_id", referencedColumnName="id")
     */
    private $excel;

    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="Report", inversedBy="excelData")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id")
     */
    private $report;
    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="QuestionCategory", inversedBy="excelData")
     * @ORM\JoinColumn(name="question_category_id", referencedColumnName="id")
     */
    private $questionCategory;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set row
     *
     * @param integer $row
     *
     * @return ExcelData
     */
    public function setRow($row)
    {
        $this->row = $row;

        return $this;
    }

    /**
     * Get row
     *
     * @return int
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set col
     *
     * @param string $col
     *
     * @return ExcelData
     */
    public function setCol($col)
    {
        $this->col = $col;

        return $this;
    }

    /**
     * Get col
     *
     * @return string
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return ExcelData
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set excel
     *
     * @param \AppBundle\Entity\Excel $excel
     *
     * @return ExcelData
     */
    public function setExcel(\AppBundle\Entity\Excel $excel = null)
    {
        $this->excel = $excel;

        return $this;
    }

    /**
     * Get excel
     *
     * @return \AppBundle\Entity\Excel
     */
    public function getExcel()
    {
        return $this->excel;
    }

    /**
     * Set report
     *
     * @param \AppBundle\Entity\Report $report
     *
     * @return ExcelData
     */
    public function setReport(\AppBundle\Entity\Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return \AppBundle\Entity\Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set questionCategory
     *
     * @param \AppBundle\Entity\QuestionCategory $questionCategory
     *
     * @return ExcelData
     */
    public function setQuestionCategory(\AppBundle\Entity\QuestionCategory $questionCategory = null)
    {
        $this->questionCategory = $questionCategory;

        return $this;
    }

    /**
     * Get questionCategory
     *
     * @return \AppBundle\Entity\QuestionCategory
     */
    public function getQuestionCategory()
    {
        return $this->questionCategory;
    }

    /**
     * Set dateValue
     *
     * @param \DateTime $dateValue
     *
     * @return ExcelData
     */
    public function setDateValue(\DateTime $dateValue)
    {
        $this->dateValue = $dateValue;

        return $this;
    }

    /**
     * Get dateValue
     *
     * @return \DateTime
     */
    public function getDateValue()
    {
        return $this->dateValue;
    }
}
