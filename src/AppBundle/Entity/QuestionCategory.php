<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * QuestionCategory
 *
 * @ORM\Table(name="question_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionCategoryRepository")
 */
class QuestionCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="use_as_list", type="boolean",options={"default"=0})
     */
    private $useAsList = false;
    /**
     * @var string
     *
     * @ORM\Column(name="rows", type="string", length=255, nullable=true)
     */
    private $rows;

    /**
     * @var string
     *
     * @ORM\Column(name="row_start", type="integer", nullable=true)
     */
    private $rowStart;

    /**
     * @var string
     *
     * @ORM\Column(name="row_end", type="integer", nullable=true)
     */
    private $rowEnd;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="Report", inversedBy="questionCategories")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id")
     */
    private $report;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="QuestionCategoryColumn", mappedBy="questionCategory", cascade={"persist", "remove"})
     */
    private $columns;

    /**
     * Creator
     * @ORM\OneToMany(targetEntity="ExcelData", mappedBy="questionCategory", cascade={"persist", "remove"})
     */
    private $excelData;
    /**
     * Creator
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SimpleValidation", mappedBy="externalList", cascade={"persist",
     *                                                                  "remove"})
     */
    private $externalListsValidations;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->columns = new ArrayCollection();
        $this->excelData = new ArrayCollection();
        $this->externalListsValidations = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return QuestionCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set rows
     *
     * @param string $rows
     *
     * @return QuestionCategory
     */
    public function setRows($rows)
    {
        $this->rows = $rows;

        return $this;
    }

    /**
     * Get rows
     *
     * @return string
     */
    public function getRows()
    {

        return $this->rows;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return QuestionCategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set report
     *
     * @param Report $report
     *
     * @return QuestionCategory
     */
    public function setReport(Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Add column
     *
     * @param QuestionCategoryColumn $column
     *
     * @return QuestionCategory
     */
    public function addColumn(QuestionCategoryColumn $column)
    {
        $this->columns[] = $column;

        return $this;
    }

    /**
     * Remove column
     *
     * @param QuestionCategoryColumn $column
     */
    public function removeColumn(QuestionCategoryColumn $column)
    {
        $this->columns->removeElement($column);
    }

    /**
     * Get columns
     *
     * @return Collection
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Set rowStart
     *
     * @param integer $rowStart
     *
     * @return QuestionCategory
     */
    public function setRowStart($rowStart)
    {
        $this->rowStart = $rowStart;

        return $this;
    }

    /**
     * Get rowStart
     *
     * @return integer
     */
    public function getRowStart()
    {
        return $this->rowStart;
    }

    /**
     * Set rowEnd
     *
     * @param integer $rowEnd
     *
     * @return QuestionCategory
     */
    public function setRowEnd($rowEnd)
    {
        $this->rowEnd = $rowEnd;

        return $this;
    }

    /**
     * Get rowEnd
     *
     * @return integer
     */
    public function getRowEnd()
    {
        return $this->rowEnd;
    }

    /**
     * Add excelDatum
     *
     * @param \AppBundle\Entity\ExcelData $excelDatum
     *
     * @return QuestionCategory
     */
    public function addExcelDatum(\AppBundle\Entity\ExcelData $excelDatum)
    {
        $this->excelData[] = $excelDatum;

        return $this;
    }

    /**
     * Remove excelDatum
     *
     * @param \AppBundle\Entity\ExcelData $excelDatum
     */
    public function removeExcelDatum(\AppBundle\Entity\ExcelData $excelDatum)
    {
        $this->excelData->removeElement($excelDatum);
    }

    /**
     * Get excelData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getExcelData()
    {
        return $this->excelData;
    }

    /**
     * Set useAsList
     *
     * @param boolean $useAsList
     *
     * @return QuestionCategory
     */
    public function setUseAsList($useAsList)
    {
        $this->useAsList = $useAsList;

        return $this;
    }

    /**
     * Get useAsList
     *
     * @return boolean
     */
    public function getUseAsList()
    {
        return $this->useAsList;
    }

    /**
     * Add externalListsValidation
     *
     * @param SimpleValidation $externalListsValidation
     *
     * @return QuestionCategory
     */
    public function addExternalListsValidation(SimpleValidation $externalListsValidation)
    {
        $this->externalListsValidations[] = $externalListsValidation;

        return $this;
    }

    /**
     * Remove externalListsValidation
     *
     * @param SimpleValidation $externalListsValidation
     */
    public function removeExternalListsValidation(SimpleValidation $externalListsValidation)
    {
        $this->externalListsValidations->removeElement($externalListsValidation);
    }

    /**
     * Get externalListsValidations
     *
     * @return Collection
     */
    public function getExternalListsValidations()
    {
        return $this->externalListsValidations;
    }
}
