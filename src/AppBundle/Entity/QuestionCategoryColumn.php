<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * QuestionCategoryColumn
 *
 * @ORM\Table(name="question_category_column")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionCategoryColumnRepository")
 */
class QuestionCategoryColumn
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="col", type="string", length=15)
     */
    private $col;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_date", type="boolean", nullable=false, options={"default" = 0})
     */
    private $isDate;

    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="QuestionCategory", inversedBy="columns")
     * @ORM\JoinColumn(name="question_category_id", referencedColumnName="id")
     */
    private $questionCategory;
    /**
     * Creator
     * @ORM\ManyToOne(targetEntity="Report", inversedBy="questionCategoryColumns")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id")
     */
    private $report;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return QuestionCategoryColumn
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set col
     *
     * @param string $col
     *
     * @return QuestionCategoryColumn
     */
    public function setCol($col)
    {
        $this->col = $col;

        return $this;
    }

    /**
     * Get col
     *
     * @return string
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * Set questionCategory
     *
     * @param QuestionCategory $questionCategory
     *
     * @return QuestionCategoryColumn
     */
    public function setQuestionCategory(QuestionCategory $questionCategory = null)
    {
        $this->questionCategory = $questionCategory;

        return $this;
    }

    /**
     * Get questionCategory
     *
     * @return QuestionCategory
     */
    public function getQuestionCategory()
    {
        return $this->questionCategory;
    }

    /**
     * Set report
     *
     * @param Report $report
     *
     * @return QuestionCategoryColumn
     */
    public function setReport(Report $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return Report
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set isDate
     *
     * @param boolean $isDate
     *
     * @return QuestionCategoryColumn
     */
    public function setIsDate($isDate)
    {
        $this->isDate = $isDate;

        return $this;
    }

    /**
     * Get isDate
     *
     * @return boolean
     */
    public function getIsDate()
    {
        return $this->isDate;
    }
}
