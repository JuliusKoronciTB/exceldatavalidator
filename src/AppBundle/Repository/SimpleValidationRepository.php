<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Category;

/**
 * SimpleValidationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SimpleValidationRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param Category $category
     * @param bool     $autoRun
     *
     * @return array
     */
    public function getAutoRunSimpleValidationRules(Category $category, $autoRun)
    {
        $qb = $this->createQueryBuilder('sv');
        $qb->where('sv.category = :category')
            ->andWhere('sv.autoRun = :autoRun')
            ->andWhere($qb->expr()->isNull('sv.externalList'));

        $qb->setParameters(
            [
                'category' => $category,
                'autoRun'  => $autoRun,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Category $category
     *
     * @param  boolean $autoRun
     *
     * @return array
     */
    public function getAutoRunExternalListSimpleValidationRules(Category $category, $autoRun)
    {
        $qb = $this->createQueryBuilder('sv');
        $qb->where('sv.category = :category')
            ->andWhere('sv.autoRun = :autoRun')
            ->andWhere($qb->expr()->isNotNull('sv.externalList'));

        $qb->setParameters(
            [
                'category' => $category,
                'autoRun'  => $autoRun,
            ]);

        return $qb->getQuery()->getResult();
    }
}
