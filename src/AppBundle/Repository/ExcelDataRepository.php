<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Excel;
use AppBundle\Entity\QuestionCategory;
use AppBundle\Entity\Report;

/**
 * ExcelDataRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ExcelDataRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param QuestionCategory $category
     * @param Excel            $excel
     *
     * @return array
     */
    public function getAllRowsForCategory(QuestionCategory $category, Excel $excel)
    {
        $rows = $this->createQueryBuilder("ed")
            ->select("DISTINCT ed.row")
            ->where('ed.questionCategory = :qc')
            ->andWhere('ed.excel = :ex')->orderBy('ed.row', 'asc');
        $rows->setParameters([
            'qc' => $category,
            'ex' => $excel,
        ]);

        $data = $rows->getQuery()->getResult();

        $return = [];
        foreach ($data as $row) {
            $return[] = $row['row'];
        }

        return $return;
    }

    /**
     * @param Excel            $excel
     * @param QuestionCategory $questionCategory
     * @param                  $row
     * @param                  $column
     *
     * @return string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getValueForColumnRow(Excel $excel, QuestionCategory $questionCategory, $row, $column)
    {
        $qb = $this->createQueryBuilder('ed')
            ->select('ed.value')
            ->where('ed.excel = :excel')
            ->andWhere('ed.questionCategory = :category')
            ->andWhere('ed.row = :row')
            ->andWhere('ed.col = :col')->setMaxResults(1);

        $qb->setParameters([
            'excel'    => $excel,
            'category' => $questionCategory,
            'row'      => $row,
            'col'      => $column,
        ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Excel  $excel
     * @param Report $report
     * @param string $coordinate
     *
     * @return null|string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getValueForValidationRule(Excel $excel, $report, $coordinate)
    {
        $qb = $this->createQueryBuilder('ed');

        $qb->select('ed.value')
            ->where('ed.excel = :excel')
            ->andWhere('ed.report = :report')
            ->andWhere($qb->expr()->eq($qb->expr()->concat('ed.col', 'ed.row'), ':coordinate'))
            ->setMaxResults(1);

        $qb->setParameters([
            'excel'      => $excel,
            'report'     => $report,
            'coordinate' => $coordinate,
        ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Excel  $excel
     * @param Report $report
     * @param string $coordinate
     *
     * @return null|string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getDateValueForValidationRule(Excel $excel, $report, $coordinate)
    {
        $qb = $this->createQueryBuilder('ed');

        $qb->select('ed.dateValue')
            ->where('ed.excel = :excel')
            ->andWhere('ed.report = :report')
            ->andWhere($qb->expr()->eq($qb->expr()->concat('ed.col', 'ed.row'), ':coordinate'))
            ->setMaxResults(1);

        $qb->setParameters([
            'excel'      => $excel,
            'report'     => $report,
            'coordinate' => $coordinate,
        ]);

        return $qb->getQuery()->getSingleScalarResult();
    }

    /**
     * @param Excel            $excel
     * @param QuestionCategory $questionCategoryGroup
     *
     * @return mixed
     */
    public function getNumberOfNotEmptyValuesInQuestionCategory(Excel $excel, QuestionCategory $questionCategoryGroup)
    {
        $qb = $this->createQueryBuilder('ed');

        $qb->select('ed')
            ->where('ed.excel = :excel')
            ->andWhere('ed.questionCategory = :questionCategory')
            ->andWhere('ed.value IS NOT NULL');

        $qb->setParameters([
            'excel'            => $excel,
            'questionCategory' => $questionCategoryGroup,
        ]);

        return $qb->getQuery()->getResult();

    }

    /**
     * @param Excel            $excel
     * @param int              $sourceQuestionCategoryId
     * @param string           $sourceColumn
     * @param QuestionCategory $externalListQuestionCategory
     * @param string           $externalListColumn
     *
     * @return array
     */
    public function findListDataInList(Excel $excel, $sourceQuestionCategoryId, $sourceColumn, Excel $externalExcel, QuestionCategory $externalListQuestionCategory, $externalListColumn)
    {
        $qb = $this->createQueryBuilder('excel_data_repository');
        $qb->select('excel_data_repository.value')
            ->where('excel_data_repository.excel = :externalExcel')
            ->andWhere('excel_data_repository.questionCategory = :externalQC')
            ->andWhere('excel_data_repository.col = :externalCol')
            ->setParameters(['externalExcel' => $externalExcel,
                             'externalQC'    => $externalListQuestionCategory,
                             'externalCol'   => $externalListColumn,]);
        $values = $qb->getQuery()->getArrayResult();
        $comp = [];

        foreach ($values as $value) {
            $comp[] = $value['value'];
        }
        unset($values);

        $qb2 = $this->createQueryBuilder('excel_data_repository2');

        return $qb2->where('excel_data_repository2.excel = :excel')
            ->andWhere('excel_data_repository2.questionCategory = :QC')
            ->andWhere('excel_data_repository2.col = :col')->andWhere($qb2->expr()->notIn('excel_data_repository2.value', $comp))
            ->setParameters([
                'excel' => $excel,
                'QC'    => $sourceQuestionCategoryId,
                'col'   => $sourceColumn,
            ])->getQuery()->getResult();

    }

    /**
     * @param Excel  $excel
     *
     * @param int    $sourceQuestionCategoryId
     * @param string $sourceColumnEmpty
     *
     * @return null|string
     */
    public function getDataWithEmptySource(Excel $excel, $sourceQuestionCategoryId, $sourceColumnEmpty)
    {
        $qb = $this->createQueryBuilder('ed');

        $qb->select('ed')
            ->where('ed.excel = :excel')
            ->andWhere('ed.questionCategory = :questionCategory')
            ->andWhere('ed.col = :col')
            ->andWhere('ed.value IS NULL')
            ->setParameters([
                'excel'            => $excel,
                'questionCategory' => $sourceQuestionCategoryId,
                'col'              => $sourceColumnEmpty,
            ]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param Excel  $excel
     *
     *
     * @param string $col
     * @param string $value
     *
     * @return null|string
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getEntityBasedOnItsValue(Excel $excel, $col, $value)
    {
        $qb = $this->createQueryBuilder('ed');

        $qb->select('ed')
            ->where('ed.excel = :excel')
            ->andWhere('ed.col = :col')
            ->andWhere('ed.value = :value')
            ->setParameters([
                'excel' => $excel,
                'value' => $value,
                'col'   => $col,
            ]);

        return $qb->getQuery()->getResult();
    }
}
