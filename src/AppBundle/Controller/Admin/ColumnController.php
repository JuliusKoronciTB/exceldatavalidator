<?php


namespace AppBundle\Controller\Admin;


use AppBundle\Entity\QuestionCategoryColumn;
use AppBundle\Form\QuestionCategoryColumnType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ColumnController
 * @Route("/cms/column")
 *
 * @package AppBundle\Controller\Admin
 */
class ColumnController extends Controller
{
    /**
     * @Route("/{questionCategoryId}", name="admin_cms_column_new")
     *
     * @param     int $questionCategoryId
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return array
     */
    public function newAction($questionCategoryId , Request $request)
    {
        $questionCategory = $this->getDoctrine()->getRepository('AppBundle:QuestionCategory')->find($questionCategoryId);

        if (null === $questionCategory) {
            throw new NotFoundHttpException('Question Category not found!');
        }

        $column = new QuestionCategoryColumn();
        $column->setQuestionCategory($questionCategory);
        $column->setReport($questionCategory->getReport());
        $form = $this->createForm(QuestionCategoryColumnType::class , $column);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($column);
            $this->getDoctrine()->getManager()->flush();

            $request->getSession()
                    ->getFlashBag()
                    ->add('success' , 'Column created!');

            return $this->redirectToRoute('admin_cms_rules_index' , ['id' => $questionCategory->getReport()->getId()]);
        }

        return $this->render('admin/column/new.html.twig' , [
            'questionGroup' => $questionCategory ,
            'form'          => $form->createView() ,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_cms_column_edit")
     *
     * @param     int $id
     * @param Request $request
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     *
     * @return array
     */
    public function editAction($id , Request $request)
    {
        $column = $this->getDoctrine()->getRepository('AppBundle:QuestionCategoryColumn')->find($id);

        if (null === $column) {
            throw new NotFoundHttpException('Question Category Column not found!');
        }

        $form = $this->createForm(QuestionCategoryColumnType::class , $column);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($column);
            $this->getDoctrine()->getManager()->flush();

            $request->getSession()
                    ->getFlashBag()
                    ->add('success' , 'Column updated!');

            return $this->redirectToRoute('admin_cms_rules_index' , ['id' => $column->getQuestionCategory()->getReport()->getId()]);
        }

        return $this->render('admin/column/edit.html.twig' , [
            'column' => $column ,
            'form'   => $form->createView() ,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_cms_question_category_column_delete")
     *
     * @param  int    $id
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($id , Request $request)
    {
        $column = $this->getDoctrine()->getRepository('AppBundle:QuestionCategoryColumn')->find($id);
        $questionCategory = $column->getQuestionCategory();

        if (null === $column) {
            throw new NotFoundHttpException('Question category column not found!');
        }

        $request->getSession()
                ->getFlashBag()
                ->add('success' , 'Column deleted!');

        $this->getDoctrine()->getManager()->remove($column);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_cms_rules_index' , ['id' => $questionCategory->getReport()->getId()]);
    }
}