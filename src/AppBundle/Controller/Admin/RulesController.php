<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Report;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class RulesController
 * @Route("/cms/rules")
 *
 * @package AppBundle\Controller\Admin
 */
class RulesController extends Controller
{
    /**
     * @Route("/{id}", name="admin_cms_rules_index")
     * @Method("GET")
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function indexAction($id)
    {
        $report = $this->getReport($id);

        return $this->render('admin/rules/index.html.twig' , [
            'report' => $report ,
        ]);
    }

    /**
     * @param string $id
     *
     * @return \AppBundle\Entity\Report|null
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getReport($id)
    {
        $report = $this->getDoctrine()->getRepository('AppBundle:Report')->find($id);

        if (null === $report) {
            throw new NotFoundHttpException('Report not found');
        }

        return $report;
    }
}
