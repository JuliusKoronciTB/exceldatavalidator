<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Services\SimpleValidationService;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\SimpleValidation;
use AppBundle\Form\SimpleValidationType;

/**
 * SimpleValidation controller.
 *
 * @Route("/cms/simplevalidation")
 */
class SimpleValidationController extends Controller
{
    /**
     * Lists all SimpleValidation entities.
     *
     * @Route("/{slug}", name="cms_simplevalidation_index")
     * @Method("GET")
     * @param string $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function indexAction($slug)
    {
        $category = $this->get('category_service')->getCategory($slug);
        $simpleAutoRunValidations = $this->get('category_service')->getAutoRunSimpleValidationRules($slug , true);
        $simpleValidations = $this->get('category_service')->getAutoRunSimpleValidationRules($slug , false);

        return $this->render('admin/simplevalidation/index.html.twig' , [
            'simpleAutoRunValidations' => $simpleAutoRunValidations ,
            'simpleValidations'        => $simpleValidations ,
            'category'                 => $category ,
        ]);
    }

    /**
     * Creates a new SimpleValidation entity.
     *
     * @Route("/new/{slug}", name="cms_simplevalidation_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param string  $slug
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function newAction(Request $request , $slug)
    {
        $simpleValidation = new SimpleValidation();
        $category = $this->get('category_service')->getCategory($slug);
        $simpleValidation->setCategory($category);
        $form = $this->createForm(SimpleValidationType::class , $simpleValidation , [
            'category' => $category ,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->checkFormRequirements($form);

            if ($form->isValid()) {
                $request->getSession()
                        ->getFlashBag()
                        ->add('success' , 'Simple Validation Rule created!');

                $em = $this->getDoctrine()->getManager();
                $em->persist($simpleValidation);
                $em->flush();

                return $this->redirectToRoute('cms_simplevalidation_index' , ['slug' => $slug]);
            }
        }

        return $this->render('admin/simplevalidation/new.html.twig' , [
            'simpleValidation' => $simpleValidation ,
            'category'         => $category ,
            'form'             => $form->createView() ,
        ]);
    }

    /**
     * Finds and displays a SimpleValidation entity.
     *
     * @Route("/show/{id}", name="cms_simplevalidation_show")
     * @Method("GET")
     * @param SimpleValidation $simpleValidation
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction(SimpleValidation $simpleValidation)
    {
        $deleteForm = $this->createDeleteForm($simpleValidation);

        return $this->render('admin/simplevalidation/show.html.twig' , [
            'simpleValidation' => $simpleValidation ,
            'delete_form'      => $deleteForm->createView() ,
        ]);
    }

    /**
     * Displays a form to edit an existing SimpleValidation entity.
     *
     * @Route("/{id}/edit", name="cms_simplevalidation_edit")
     * @Method({"GET", "POST"})
     * @param Request          $request
     * @param SimpleValidation $simpleValidation
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \OutOfBoundsException
     * @throws \LogicException
     */
    public function editAction(Request $request , SimpleValidation $simpleValidation)
    {
        $deleteForm = $this->createDeleteForm($simpleValidation);
        $editForm = $this->createForm(SimpleValidationType::class , $simpleValidation , [
            'category' => $simpleValidation->getCategory() ,
        ]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted()) {
            $this->checkFormRequirements($editForm);

            if ($editForm->isValid()) {
                $request->getSession()
                        ->getFlashBag()
                        ->add('success' , 'Simple Validation Rule edited!');
                $em = $this->getDoctrine()->getManager();
                $em->persist($simpleValidation);
                $em->flush();

                return $this->redirectToRoute('cms_simplevalidation_index' , ['slug' => $simpleValidation->getCategory()->getSlug()]);
            }
        }

        return $this->render('admin/simplevalidation/edit.html.twig' , [
            'simpleValidation' => $simpleValidation ,
            'edit_form'        => $editForm->createView() ,
            'delete_form'      => $deleteForm->createView() ,
        ]);
    }

    /**
     * Deletes a SimpleValidation entity.
     *
     * @Route("/{id}", name="cms_simplevalidation_delete")
     * @Method("DELETE")
     * @param Request          $request
     * @param SimpleValidation $simpleValidation
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     */
    public function deleteAction(Request $request , SimpleValidation $simpleValidation)
    {
        $slug = $simpleValidation->getCategory()->getSlug();
        $form = $this->createDeleteForm($simpleValidation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $request->getSession()
                    ->getFlashBag()
                    ->add('success' , 'Simple Validation Rule deleted!');
            $em = $this->getDoctrine()->getManager();
            $em->remove($simpleValidation);
            $em->flush();
        }

        return $this->redirectToRoute('cms_simplevalidation_index' , ['slug' => $slug]);
    }

    /**
     * Creates a form to delete a SimpleValidation entity.
     *
     * @param SimpleValidation $simpleValidation The SimpleValidation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SimpleValidation $simpleValidation)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('cms_simplevalidation_delete' , ['id' => $simpleValidation->getId()]))
                    ->setMethod('DELETE')
                    ->getForm();
    }

    /**
     * @param $form
     *
     * @throws \OutOfBoundsException
     */
    private function checkFormRequirements(Form $form)
    {
        $type = $form->get('type')->getData();

        switch ($type) {
            case SimpleValidationService::TYPE_FILE_EXTENSION:
                $compareTo = $form->get('compareTo')->getData();
                if (!$compareTo) {
                    $form->get('compareTo')->addError(new FormError('Please, provide file extension e.g. xls'));
                }
                break;
            case SimpleValidationService::TYPE_EQUALS_TO_FIELD:
            case SimpleValidationService::TYPE_DOES_NOT_EQUALS_TO_FIELD:
                $sourceField = $form->get('sourceField')->getData();
                $sourceSheet = $form->get('sourceSheet')->getData();
                $compareField = $form->get('compareTo')->getData();
                $compareSheet = $form->get('compareToSheet')->getData();

                if (!$sourceField) {
                    $form->get('sourceField')->addError(new FormError('Please, provide source cell coordinates e.g A3'));
                }
                if (!$sourceSheet) {
                    $form->get('sourceSheet')->addError(new FormError('Please, provide source sheet of source cell'));
                }
                if (!$compareField) {
                    $form->get('compareTo')->addError(new FormError('Please, provide cell coordinates e.g A3 for comparison'));
                }
                if (!$compareSheet) {
                    $form->get('compareToSheet')->addError(new FormError('Please, provide source sheet of comparison cell'));
                }
                break;
            case SimpleValidationService::TYPE_EQUALS_TO_VALUE:
            case SimpleValidationService::TYPE_DOES_NOT_EQUALS_TO_VALUE:
            case SimpleValidationService::TYPE_CONTAINS_SYMBOL:
                $sourceField = $form->get('sourceField')->getData();
                $sourceSheet = $form->get('sourceSheet')->getData();
                $compareValue = $form->get('compareTo')->getData();

                if (!$sourceField) {
                    $form->get('sourceField')->addError(new FormError('Please, provide source cell coordinates e.g A3'));
                }
                if (!$sourceSheet) {
                    $form->get('sourceSheet')->addError(new FormError('Please, provide source sheet of source cell'));
                }
                if (!$compareValue) {
                    $form->get('compareTo')->addError(new FormError('Please, provide value/symbol for comparison'));
                }
                break;
            case SimpleValidationService::TYPE_IN_LIST:
                $sourceField = $form->get('sourceField')->getData();
                $sourceSheet = $form->get('sourceSheet')->getData();
                $compareValue = $form->get('compareTo')->getData();

                if (!$sourceField) {
                    $form->get('sourceField')->addError(new FormError('Please, provide source cell coordinates e.g A3'));
                }
                if (!$sourceSheet) {
                    $form->get('sourceSheet')->addError(new FormError('Please, provide source sheet of source cell'));
                }
                if (!$compareValue) {
                    $form->get('compareTo')->addError(new FormError('Please,  provide a coma separated list of values e.g. yes,no'));
                }
                break;
            case SimpleValidationService::TYPE_NOT_BLANK:
            case SimpleValidationService::TYPE_IS_BLANK:
            case SimpleValidationService::TYPE_IS_EMAIL:
            case SimpleValidationService::TYPE_IS_DATE:
            case SimpleValidationService::TYPE_IS_DATE_IN_FUTURE:
            case SimpleValidationService::TYPE_IS_DATE_IN_PAST:
            case SimpleValidationService::TYPE_IS_DATE_IN_FORMAT_DD_MM_YYYY:
                $sourceField = $form->get('sourceField')->getData();
                $sourceSheet = $form->get('sourceSheet')->getData();

                if (!$sourceField) {
                    $form->get('sourceField')->addError(new FormError('Please, provide source cell coordinates e.g A3'));
                }
                if (!$sourceSheet) {
                    $form->get('sourceSheet')->addError(new FormError('Please, provide source sheet of source cell'));
                }
                break;
            case SimpleValidationService::TYPE_IS_DATE_OLDER_THAN_MONTHS:
            case SimpleValidationService::TYPE_IS_DATE_NEWER_THAN_MONTHS:
                $sourceField = $form->get('sourceField')->getData();
                $sourceSheet = $form->get('sourceSheet')->getData();
                $compareValue = $form->get('compareTo')->getData();

                if (!$sourceField) {
                    $form->get('sourceField')->addError(new FormError('Please, provide source cell coordinates e.g A3'));
                }
                if (!$sourceSheet) {
                    $form->get('sourceSheet')->addError(new FormError('Please, provide source sheet of source cell'));
                }
                if (!$compareValue) {
                    $form->get('compareTo')->addError(new FormError('Please,  provide a number of months to comparison e.g. 6'));
                }
                break;
            case SimpleValidationService::TYPE_LIST_NOT_BLANK:
            case SimpleValidationService::TYPE_LIST_IS_BLANK:
                $sourceList = $form->get('sourceList')->getData();

                if (!$sourceList) {
                    $form->get('sourceList')->addError(new FormError('Please, select a source list'));
                }
                break;
            case SimpleValidationService::TYPE_LIST_GREATER_THAN:
                $sourceList = $form->get('sourceList')->getData();
                $compareTo = $form->get('compareTo')->getData();

                if (!$sourceList) {
                    $form->get('sourceList')->addError(new FormError('Please, select a source list'));
                }

                if (!$compareTo) {
                    $form->get('compareTo')->addError(new FormError('Please,  provide a number to compare to with'));
                }
                break;
        }
    }
}
