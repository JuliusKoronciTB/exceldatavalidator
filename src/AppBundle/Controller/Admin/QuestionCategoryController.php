<?php


namespace AppBundle\Controller\Admin;

use AppBundle\Entity\QuestionCategory;
use AppBundle\Entity\Report;
use AppBundle\Form\QuestionCategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class QuestionCategoryController
 * @Route("/cms/question-category")
 *
 * @package AppBundle\Controller\Admin
 */
class QuestionCategoryController extends Controller
{
    /**
     * @Route("/{id}", name="admin_cms_question_category_new")
     *
     * @param int     $id
     *
     * @param Request $request
     *
     * @throws \LogicException
     *
     * @return array|RedirectResponse
     * @throws \OutOfBoundsException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function newAction($id , Request $request)
    {
        $report = $this->getReport($id);

        $questionCategory = new QuestionCategory();
        $questionCategory->setReport($report);

        $form = $this->createForm(QuestionCategoryType::class , $questionCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $formRows = $form->get('rows')->getData();
            if ($formRows) {
                try {
                    $rows = $this->processRows($formRows);
                    $questionCategory->setRows($rows);
                } catch (\UnexpectedValueException $e) {
                    $form->get('rows')->addError(new FormError('Invalid rows values!'));
                }
            } else {
                if (!$form->get('rowStart')->getData()) {
                    $form->get('rows')->addError(new FormError('Rows or Rows Start must be entered!'));
                }
            }
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($questionCategory);
                $this->getDoctrine()->getManager()->flush();
                $request->getSession()
                        ->getFlashBag()
                        ->add('success' , 'Question group created!');

                return $this->redirectToRoute('admin_cms_rules_index' , ['id' => $id]);
            }
        }

        return $this->render('admin/questionCategory/new.html.twig' , [
            'report' => $report ,
            'form'   => $form->createView() ,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_cms_question_category_edit")
     *
     * @param int     $id
     *
     * @param Request $request
     *
     * @throws \LogicException
     *
     * @return array|RedirectResponse
     * @throws \OutOfBoundsException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editAction($id , Request $request)
    {
        $questionCategory = $this->getDoctrine()->getRepository('AppBundle:QuestionCategory')->find($id);

        if (null === $questionCategory) {
            throw new NotFoundHttpException('Question category not found!');
        }

        $form = $this->createForm(QuestionCategoryType::class , $questionCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $formRows = $form->get('rows')->getData();
            if ($formRows) {
                try {
                    $rows = $this->processRows($formRows);
                    $questionCategory->setRows($rows);
                } catch (\UnexpectedValueException $e) {
                    $form->get('rows')->addError(new FormError('Invalid rows values!'));
                }
            } else {
                if (!$form->get('rowStart')->getData()) {
                    $form->get('rows')->addError(new FormError('Rows or Rows Start must be entered!'));
                }
            }
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->persist($questionCategory);
                $this->getDoctrine()->getManager()->flush();
                $request->getSession()
                        ->getFlashBag()
                        ->add('success' , 'Question group edited!');

                return $this->redirectToRoute('admin_cms_rules_index' , ['id' => $questionCategory->getReport()->getId()]);
            }
        }

        return $this->render('admin/questionCategory/edit.html.twig' , [
            'questionCategory' => $questionCategory ,
            'form'             => $form->createView() ,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_cms_question_category_delete")
     *
     * @param  int    $id
     * @param Request $request
     *
     * @return RedirectResponse
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($id , Request $request)
    {
        $questionCategory = $this->getDoctrine()->getRepository('AppBundle:QuestionCategory')->find($id);
        $reportId = $questionCategory->getReport()->getId();

        if (null === $questionCategory) {
            throw new NotFoundHttpException('Question category not found!');
        }

        $request->getSession()
                ->getFlashBag()
                ->add('success' , 'Question Group deleted!');

        $this->getDoctrine()->getManager()->remove($questionCategory);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_cms_rules_index' , ['id' => $reportId]);
    }

    /**
     * @param string $id
     *
     * @return \AppBundle\Entity\Report|null
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     *
     * @return Report
     */
    private function getReport($id)
    {
        $report = $this->getDoctrine()->getRepository('AppBundle:Report')->find($id);

        if (null === $report) {
            throw new NotFoundHttpException('Report not found');
        }

        return $report;
    }

    /**
     * @param string|array $rows
     *
     * @return array
     * @throws \Exception
     */
    private function processRows($rows)
    {
        $rows = str_replace(' ' , '' , $rows);
        $rows = str_replace(';' , ',' , $rows);
        $rows = str_replace('' , '' , $rows);

        $rows = explode(',' , $rows);

        $return = [];
        foreach ($rows as $row) {
            if (is_numeric($row)) {
                $return[] = (int) $row;
            }
        }

        if (count($return) === 0) {
            throw new \UnexpectedValueException('Invalid values');
        }

        return implode(',' , $return);
    }
}