<?php

namespace AppBundle\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 * @Route("/cms/validation")
 *
 * @package AppBundle\Controller\Admin
 */
class ValidationController extends Controller
{
    /**
     * @Route("/{slug}", name="admin_cms_validation_index")
     * @param string $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function indexAction($slug)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->findOneBy(['slug' => $slug]);

        if (null === $category) {
            throw new NotFoundHttpException('Category not found');
        }

        return $this->render('admin/validation/index.html.twig' , [
            'category' => $category,
        ]);
    }
}
