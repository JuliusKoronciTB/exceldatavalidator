<?php


namespace AppBundle\Controller\Admin;


use AppBundle\Entity\AdvancedValidation;
use AppBundle\Form\AdvancedValidationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class AdvancedValidationController
 *
 * @Route("/cms/advancedvalidation")
 */
class AdvancedValidationController extends Controller
{
    /**
     * Lists all AdvancedValidation entities.
     *
     * @Route("/{slug}", name="cms_advancedvalidation_index")
     * @Method("GET")
     * @param string $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function indexAction($slug)
    {
        $category = $this->get('category_service')->getCategory($slug);
        $advancedValidations = $category->getAdvancedValidations();

        return $this->render('admin/advancedvalidation/index.html.twig' , [
            'advancedValidations' => $advancedValidations ,
            'category'            => $category ,
        ]);
    }

    /**
     * Creates a new AdvancedValidation entity.
     *
     * @Route("/new/{slug}", name="cms_advancedvalidation_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param string  $slug
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \OutOfBoundsException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function newAction(Request $request , $slug)
    {
        $advancedValidation = new AdvancedValidation();
        $category = $this->get('category_service')->getCategory($slug);
        $advancedValidation->setCategory($category);

        $form = $this->createForm(AdvancedValidationType::class , $advancedValidation , [
            'category' => $category ,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if (false === $this->checkNoCircularReference($advancedValidation)) {
                $form->get('childrenAdvancedValidations')->addError(new FormError('Circular reference introduced'));
            }

            if ($form->isValid()) {
                $request->getSession()
                        ->getFlashBag()
                        ->add('success' , 'Advanced Validation Rule created!');

                $em = $this->getDoctrine()->getManager();
                $em->persist($advancedValidation);
                $em->flush();

                return $this->redirectToRoute('cms_advancedvalidation_index' , ['slug' => $slug]);
            }
        }

        return $this->render('admin/advancedvalidation/new.html.twig' , [
            'advancedValidation' => $advancedValidation ,
            'form'               => $form->createView() ,
            'category'           => $category ,
        ]);
    }

    /**
     * Creates a new AdvancedValidation entity.
     *
     * @Route("/edit/{id}", name="cms_advancedvalidation_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param int     $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \OutOfBoundsException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function editAction(Request $request , $id)
    {
        $advancedValidation = $this->getDoctrine()->getRepository('AppBundle:AdvancedValidation')->find($id);
        if (null === $advancedValidation) {
            throw new NotFoundHttpException('Advanced validation not found');
        }
        $deleteForm = $this->createDeleteForm($advancedValidation);

        $form = $this->createForm(AdvancedValidationType::class , $advancedValidation , [
            'category' => $advancedValidation->getCategory() ,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            if (false === $this->checkNoCircularReference($advancedValidation)) {
                $form->get('childrenAdvancedValidations')->addError(new FormError('Circular reference introduced'));
            }

            if ($form->isValid()) {
                $request->getSession()
                        ->getFlashBag()
                        ->add('success' , 'Advanced Validation Rule edited!');
                $em = $this->getDoctrine()->getManager();
                $em->persist($advancedValidation);
                $em->flush();

                return $this->redirectToRoute('cms_advancedvalidation_index' , ['slug' => $advancedValidation->getCategory()->getSlug()]);
            }
        }

        return $this->render('admin/advancedvalidation/edit.html.twig' , [
            'advancedValidation' => $advancedValidation ,
            'form'               => $form->createView() ,
            'category'           => $advancedValidation->getCategory() ,
            'delete_form'        => $deleteForm->createView() ,
        ]);
    }

    /**
     * Deletes a AdvancedValidation entity.
     *
     * @Route("/{id}", name="cms_advancedvalidation_delete")
     * @Method("DELETE")
     * @param Request            $request
     *
     * @param AdvancedValidation $advancedValidation
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     */
    public function deleteAction(Request $request , AdvancedValidation $advancedValidation)
    {
        $slug = $advancedValidation->getCategory()->getSlug();
        $form = $this->createDeleteForm($advancedValidation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $request->getSession()
                    ->getFlashBag()
                    ->add('success' , 'Advanced Validation Rule deleted!');

            $em = $this->getDoctrine()->getManager();
            $em->remove($advancedValidation);
            $em->flush();
        }

        return $this->redirectToRoute('cms_advancedvalidation_index' , ['slug' => $slug]);
    }

    /**
     * Creates a form to delete a AdvancedValidation entity.
     *
     * @param AdvancedValidation $advancedValidation
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AdvancedValidation $advancedValidation)
    {
        return $this->createFormBuilder()
                    ->setAction($this->generateUrl('cms_advancedvalidation_delete' , ['id' => $advancedValidation->getId()]))
                    ->setMethod('DELETE')
                    ->getForm();
    }

    /**
     * Returns false if circular reference found
     *
     * @param AdvancedValidation $validation
     *
     * @return bool
     */
    private function checkNoCircularReference(AdvancedValidation $validation)
    {
        $parents = [];

        try {
            $this->startRecursions($parents , $validation);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * @param array              $parents
     * @param AdvancedValidation $validation
     *
     * @throws \Exception
     */
    private function startRecursions(array &$parents , AdvancedValidation $validation)
    {
        if (!in_array($validation->getId() , $parents)) {
            $parents[] = $validation->getId();
            foreach ($validation->getChildrenAdvancedValidations() as $val) {
                $this->startRecursions($parents , $val);
            }
        } else {
            throw new \Exception('Circular reference');
        }
    }
}