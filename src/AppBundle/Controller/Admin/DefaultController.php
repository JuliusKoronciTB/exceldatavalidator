<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\ExcelData;
use AppBundle\Entity\QuestionCategory;
use AppBundle\Entity\QuestionCategoryColumn;
use AppBundle\Entity\Report;
use AppBundle\Form\ExcelType;
use AppBundle\Entity\Excel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 *
 * @package AppBundle\Controller\Admin
 */
class DefaultController extends Controller
{

    /**
     * @Route("/{id}", name="excel_show", requirements={"id"="\d+"})
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \UnexpectedValueException
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function showAction($id)
    {
        $excel = $this->getDoctrine()->getRepository('AppBundle:Excel')->find($id);
        if (null === $excel) {
            throw new NotFoundHttpException('Excel not found');
        }

        return $this->render('admin/default/show.html.twig', [
            'excel'   => $excel,
            'reports' => $this->getDoctrine()->getRepository('AppBundle:Report')->findBy([
                'category' => $excel->getCategory(),
            ]),
        ]);
    }

    /**
     * @Route("/{slug}", name="admin_homepage", defaults={"slug"="cfsi-cmrt"})
     * @Route("/", name="admin_homepage_f", defaults={"slug"="cfsi-cmrt"})
     * @param $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \UnexpectedValueException
     * @throws \LogicException
     */
    public function homeAction($slug = 'cfsi-cmrt')
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->findOneBy(['slug' => $slug]);

        return $this->render('admin/default/home.html.twig', [
            'category' => $category,
            'excels'   => $this->getDoctrine()->getRepository('AppBundle:Excel')->findBy(['category' => $category], ['id' => 'desc'], 12),
        ]);
    }


    /**
     * @Route("/{id}/processed-data", name="excel_show_processed_data", requirements={"id"="\d+"})
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \UnexpectedValueException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function showProcessedDataAction($id)
    {
        $excel = $this->getDoctrine()->getRepository('AppBundle:Excel')->find($id);
        if (null === $excel) {
            throw new NotFoundHttpException('Excel not found');
        }

        return $this->render('admin/default/showProcessedData.html.twig', [
            'excel'   => $excel,
            'reports' => $this->getDoctrine()->getRepository('AppBundle:Report')->findBy([
                'category' => $excel->getCategory(),
            ]),
        ]);
    }

    /**
     * @Route("/{id}/report-from-validation", name="excel_show_report_from_validation", requirements={"id"="\d+"})
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function showReportFromValidationAction($id)
    {
        $excel = $this->getDoctrine()->getRepository('AppBundle:Excel')->find($id);
        if (null === $excel) {
            throw new NotFoundHttpException('Excel not found');
        }

        $reportLog = $excel->getReportLog();
        $errors = $reportLog['errors'];
        $logs = $reportLog['logs'];

        return $this->render('admin/default/showReportFromValidation.html.twig', [
            'excel'  => $excel,
            'errors' => $errors,
            'logs'   => $logs,
        ]);
    }

    /**
     * @Route("/validate/{id}", name="excel_validate", requirements={"id"="\d+"})
     * @param int     $id
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NoResultException
     * @throws \LogicException
     */
    public function validateAction($id, Request $request)
    {
        $excel = $this->getDoctrine()->getRepository('AppBundle:Excel')->find($id);
        if (null === $excel) {
            throw new NotFoundHttpException('Excel not found');
        }
        $logs1 = $this->get('simple_validation_service')->validate($excel);
        $logs2 = $this->get('advanced_validation_service')->validate($excel);
        $logs3 = $this->get('external_list_validation_service')->validate($excel);

        $logs['logs'] = array_merge($logs1['logs'], $logs2['logs'], $logs3['logs']);
        $logs['errors'] = array_merge($logs1['errors'], $logs2['errors'], $logs3['errors']);

//        $excel->setValidated(true);
        $excel->setReportLog($logs);

        if (0 === count($logs['errors'])) {
            $excel->setValid(true);
            $this->getDoctrine()->getManager()->persist($excel);
            $this->getDoctrine()->getManager()->flush();

            if (!$request->isXmlHttpRequest()) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'Data successfully validated!');

                return $this->redirectToRoute('admin_homepage');
            }

            return new JsonResponse([
                'success' => true,
                'message' => 'Data successfully validated',
                'url'     => $this->generateUrl('excel_show_report_from_validation', ['id' => $id]),
            ]);
        }

        $this->getDoctrine()->getManager()->persist($excel);
        $this->getDoctrine()->getManager()->flush();

        if (!$request->isXmlHttpRequest()) {
            $request->getSession()
                ->getFlashBag()
                ->add('danger', 'Validation not successful!');

            return $this->redirectToRoute('admin_homepage');
        }

        return new JsonResponse([
            'success' => false,
            'message' => ('Validation not successful!'),
            'url'     => $this->generateUrl('excel_show_report_from_validation', ['id' => $id]),
        ], 500);
    }

    /**
     * @Route("/process/{id}", name="excel_process", requirements={"id"="\d+"})
     * @param  int    $id
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function loadExcelToDbAction($id, Request $request)
    {
        $excel = $this->getDoctrine()->getRepository('AppBundle:Excel')->find($id);
        if (null === $excel) {
            throw new NotFoundHttpException('Excel not found');
        }
        $inputFileName = $this->getParameter('excel_dir') . DIRECTORY_SEPARATOR . $excel->getLocation();
        try {
            $reports = $excel->getCategory()->getReports();

            $this->getDoctrine()->getConnection()->beginTransaction();
            $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
            $cacheSettings = [' memoryCacheSize ' => '20MB'];
            \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
            $inputFileType = \PHPExcel_IOFactory::identify($inputFileName);
            $objReader = \PHPExcel_IOFactory::createReader($inputFileType);
            $objReader->setReadDataOnly(true);
//                $objReader->setLoadSheetsOnly($report->getSheetName());
            $objPHPExcel = $objReader->load($inputFileName);
            /** @var Report $report */
            foreach ($reports as $report) {

                $objPHPExcel->setActiveSheetIndexByName($report->getSheetName());
                /** @var Report $report */
                $categories = $report->getQuestionCategories();
                /** @var QuestionCategory $category */
                foreach ($categories as $category) {

                    if (!empty($category->getRows())) {
                        $rows = explode(',', $category->getRows());
                        $this->handleRows($category, $objPHPExcel, $rows, $excel);
                    } elseif (null !== $category->getRowEnd()) {
                        $this->handleStartEndRows($category, $objPHPExcel, $excel);
                    } else {
                        $this->handleStartNoEndRows($category, $objPHPExcel, $excel);
                    }
                }
                gc_collect_cycles();
            }
        } catch (\Exception $e) {
            $this->getDoctrine()->getConnection()->rollback();

            if (!$request->isXmlHttpRequest()) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('danger', 'Processing of data Filed!');

                return $this->redirectToRoute('admin_homepage');
            }

            return new JsonResponse([
                'success' => false,
                'message' => $e->getMessage() . ' failed processing data, please make sure your Excel is in xls(97-2004) format. It is not enough to just change the extension! Please open your excel and save as xls format and try again.',
            ], 500);
        }

        $this->getDoctrine()->getConnection()->commit();
        $excel->setProcessed(true);
        $this->getDoctrine()->getManager()->persist($excel);
        $this->getDoctrine()->getManager()->flush();

        if (!$request->isXmlHttpRequest()) {
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'Data were successfully processed!');

            return $this->redirectToRoute('admin_homepage');
        }

        return new JsonResponse([
            'success' => true,
            'message' => 'data successfully processed',
        ]);
    }

    /**
     * @Route("/upload/{slug}", name="admin_upload")
     * @param Request $request
     *
     * @param string  $slug
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     * @throws \OutOfBoundsException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function uploadAction(Request $request, $slug)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppBundle:Category')->findOneBy([
            'slug' => $slug,
        ]);
        if (null === $category) {
            throw new NotFoundHttpException('Category Not Found');
        }
        $spreadsheet = new Excel();
        $spreadsheet->setCategory($category);
        $spreadsheet->setUser($this->getUser());
        $form = $this->createForm(ExcelType::class, $spreadsheet);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {

            /** @var UploadedFile $imageFile */
            $xls = $form->get('file')->getData();
            $extension = $xls->guessExtension();
            if (!$xls || !in_array($extension, ['xls', 'xlsx'])) {
                $form->get('file')->addError(new FormError('Upload a valid xls file'));
            }
            if ($form->isValid()) {

                $this->addFileToExcel($spreadsheet, $xls);

                $em->persist($spreadsheet);
                $em->flush();

                return $this->redirectToRoute('excel_show', ['id' => $spreadsheet->getId()]);
            }
        }

        return $this->render('admin/default/upload.html.twig', [
            'upload_form' => $form->createView(),
        ]);
    }

    /**
     * @param Excel        $excel
     * @param UploadedFile $xsl
     *
     * @throws \Symfony\Component\HttpFoundation\File\Exception\FileException
     */
    private function addFileToExcel(Excel $excel, UploadedFile $xsl)
    {
        $fileName = $xsl->getClientOriginalName();
        $bad = array_merge(
            array_map('chr', range(0, 31)),
            ["<", ">", ":", '"', " / ", "\\", " | ", " ", " ? ", " * "]);
        $fileName = str_replace($bad, "", $fileName);
        $targetDir = $this->getUploadDir();

        $xsl->move($targetDir['path'], $fileName);

        $excel->setLocation($targetDir['dir'] . '/' . $fileName);
        $excel->setFilename($fileName);
    }


    /**
     * Get a relatively unique folder name
     *
     * @param bool $onlyName
     *
     * @return string|array folder name or path and directory name ('path'=> '', 'dir'=>'')
     */
    private function getUploadDir($onlyName = false)
    {
        $characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" . time();

        $unique = substr(md5(str_shuffle($characters)), 1, 13);
        if ($onlyName) {
            return $unique;
        }
        $uploadDir = $this->getParameter('excel_dir');
        $uploadDir = $uploadDir . DIRECTORY_SEPARATOR . $unique;

        if (!is_dir($uploadDir)) {
            @mkdir($uploadDir, 0777, true);
        }

        return [
            'path' => $uploadDir,
            'dir'  => $unique,
        ];
    }

    /**
     * @param QuestionCategory $category
     * @param \PHPExcel        $objPHPExcel
     * @param array            $rows
     * @param Excel            $excel
     *
     * @throws \LogicException
     */
    private function handleRows(QuestionCategory $category, \PHPExcel $objPHPExcel, array $rows, Excel $excel)
    {
        $count = 0;
        foreach ($rows as $row) {
            $response = $this->processRow($category, $objPHPExcel, $row, $excel);
            if ($count % 50 === 0) {
                $this->getDoctrine()->getManager()->flush();
                gc_collect_cycles();
            }
            $count++;
        }

        $this->getDoctrine()->getManager()->flush();
    }

    /**
     * @param QuestionCategory $category
     * @param \PHPExcel        $objPHPExcel
     * @param Excel            $excel
     *
     * @throws \LogicException
     */
    private function handleStartEndRows(QuestionCategory $category, \PHPExcel $objPHPExcel, Excel $excel)
    {
        $count = 0;
        $limit = $category->getRowEnd() - $category->getRowStart() + 1;
        $start = $category->getRowStart();
        while ($count < $limit) {
            $response = $this->processRow($category, $objPHPExcel, $start, $excel);
            $start++;
            if ($count % 50 === 0) {
                $this->getDoctrine()->getManager()->flush();
                gc_collect_cycles();
            }
            $count++;

            if ($count > 3000) {
                break;
            }
            $response = false;
        }
    }

    /**
     * @param QuestionCategory $category
     * @param \PHPExcel        $objPHPExcel
     * @param Excel            $excel
     *
     * @throws \LogicException
     */
    private function handleStartNoEndRows(QuestionCategory $category, \PHPExcel $objPHPExcel, Excel $excel)
    {
        $limit = 3000;
        $row = $category->getRowStart();
        $count = 0;
        $emptyRowCount = 0;

        while ($count < $limit) {
            $response = $this->processRow($category, $objPHPExcel, $row, $excel);
            $row++;
            $count++;
            if (false === $response) {
                $emptyRowCount++;
            }
            if ($emptyRowCount >= 2) {
                break;
            }
            if ($count % 50 === 0) {
                $this->getDoctrine()->getManager()->flush();
                gc_collect_cycles();
            }
            $response = false;
        }

        $row = false;
    }

    private function processRow(QuestionCategory $category, \PHPExcel $objPHPExcel, $row, Excel $excel)
    {

        $columns = $category->getColumns();
        $columnCount = count($columns);
        $emptyColumnCount = 0;

        /** @var QuestionCategoryColumn $column */
        foreach ($columns as $column) {
            $coordinate = strtoupper($column->getCol() . $row);
            try {
                $cell = $objPHPExcel->getActiveSheet()->getCell($coordinate);
                if ($column->getIsDate()) {
                    try {
                        $value = $cell->getFormattedValue();
                        if (!empty($value)) {
                            $dateValueFirst = \PHPExcel_Shared_Date::ExcelToPHPObject($cell->getValue());
                            if ($dateValueFirst instanceof \DateTime) {
                                $dateValue = $dateValueFirst;
                                $value = $dateValueFirst->format('d-M-Y');
                            }
                        }
                    } catch (\Exception $e) {
                    }
                } else {
                    $value = $cell->getOldCalculatedValue();
                    if (empty($value)) {
                        $value = $cell->getFormattedValue();
                        if (empty($value)) {
                            $value = $cell->getValue();
                            if (empty($value) || substr($value, 0, 1) == '=') {
                                $emptyColumnCount++;
                            }
                        }
                    }
                }
            } catch (\Exception $e) {
                $emptyColumnCount++;
            }
            if ($emptyColumnCount === $columnCount) {
                return false;
            }
            $item = new ExcelData();
            $item->setCol($column->getCol());
            $item->setRow($row);
            if (!empty($value)) {
                $item->setValue($value);
            }
            if (!empty($dateValue)) {
                $item->setDateValue($dateValue);
            }
            $item->setExcel($excel);
            $item->setQuestionCategory($category);
            $item->setReport($category->getReport());

            $this->getDoctrine()->getManager()->persist($item);

            $item = false;
            $column = false;
            $coordinate = false;
            $value = false;
            gc_collect_cycles();


        }
        $columns = false;

        return true;
    }
}
