<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Report;
use AppBundle\Form\ReportType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 * @Route("/cms/sheet")
 *
 * @package AppBundle\Controller\Admin
 */
class SheetController extends Controller
{
    /**
     * @Route("/{slug}", name="admin_cms_spreadsheet_index")
     * @Method("GET")
     * @param string $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function indexAction($slug)
    {
        $category = $this->getCategory($slug);

        return $this->render('admin/sheet/index.html.twig' , [
            'category' => $category ,
        ]);
    }

    /**
     * @Route("/new/{slug}", name="admin_cms_spreadsheet_new")
     *
     * @param string  $slug
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function newSheetAction($slug , Request $request)
    {
        $category = $this->getCategory($slug);

        $report = new Report();
        $report->setCategory($category);

        $form = $this->createForm(ReportType::class , $report);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($report);
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                    ->getFlashBag()
                    ->add('success' , 'New sheet added!');

            return $this->redirectToRoute('admin_cms_spreadsheet_index' , ['slug' => $slug]);
        }

        return $this->render('admin/sheet/new.html.twig' , [
            'category' => $category ,
            'form'     => $form->createView() ,
        ]);
    }

    /**
     * @Route("/edit/{slug}/{id}", name="admin_cms_spreadsheet_edit", requirements={"id"="\d+"})
     *
     * @param string  $slug
     *
     * @param  int    $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editSheetAction($slug , $id , Request $request)
    {
        $category = $this->getCategory($slug);

        $report = $this->getDoctrine()->getRepository('AppBundle:Report')->find($id);

        if (null === $report) {
            throw new NotFoundHttpException('Report not found');
        }

        $report->setCategory($category);

        $form = $this->createForm(ReportType::class , $report);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($report);
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                    ->getFlashBag()
                    ->add('success' , 'Sheet updated!');

            return $this->redirectToRoute('admin_cms_spreadsheet_index' , ['slug' => $slug]);
        }

        return $this->render('admin/sheet/edit.html.twig' , [
            'category' => $category ,
            'report'   => $report ,
            'form'     => $form->createView() ,
        ]);
    }

    /**
     * @Route("/delete/{slug}/{id}", name="admin_cms_spreadsheet_delete", requirements={"id"="\d+"})
     * @param string  $slug
     * @param int     $id
     *
     * @param Request $request
     *
     * @return array
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($slug , $id , Request $request)
    {
        $report = $this->getDoctrine()->getRepository('AppBundle:Report')->find($id);
        if (null === $report) {
            throw new NotFoundHttpException('Report not found');
        }
        $request->getSession()
                ->getFlashBag()
                ->add('success' , 'Sheet deleted!');
        $this->getDoctrine()->getManager()->remove($report);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_cms_spreadsheet_index' , ['slug' => $slug]);
    }

    /**
     * @param string $slug
     *
     * @return \AppBundle\Entity\Category
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    private function getCategory($slug)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->findOneBy([
            'slug' => $slug ,
        ]);

        if (null === $category) {
            throw new NotFoundHttpException('Category not found');
        }

        return $category;
    }
}
