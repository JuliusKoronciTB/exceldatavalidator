<?php


namespace AppBundle\Controller\Admin;


use AppBundle\Entity\AdvancedValidation;
use AppBundle\Entity\QuestionCategory;
use AppBundle\Entity\Report;
use AppBundle\Entity\SimpleValidation;
use AppBundle\Form\AdvancedValidationType;
use AppBundle\Form\ExternalListValidation;
use AppBundle\Services\ExternalListValidationService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ListValidationController
 *
 * @Route("/cms/listvalidation")
 */
class ListValidationController extends Controller
{
    /**
     * Lists all ListValidation entities.
     *
     * @Route("/{slug}", name="cms_listvalidation_index")
     * @Method("GET")
     * @param string $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function indexAction($slug)
    {
        $category = $this->get('category_service')->getCategory($slug);

        $autoRunValidations = $this->getDoctrine()->getRepository('AppBundle:SimpleValidation')->getAutoRunExternalListSimpleValidationRules($category, true);
        $notAutoRunValidations = $this->getDoctrine()->getRepository('AppBundle:SimpleValidation')->getAutoRunExternalListSimpleValidationRules($category, false);


        return $this->render('admin/listvalidation/index.html.twig', [
            'category'              => $category,
            'autoRunValidations'    => $autoRunValidations,
            'notAutoRunValidations' => $notAutoRunValidations,
        ]);
    }

    /**
     * Lists all AdvancedValidation entities.
     *
     * @Route("/new/{slug}", name="cms_listvalidation_new")
     * @param Request $request
     * @param string  $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \OutOfBoundsException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function newAction(Request $request, $slug)
    {
        $simpleValidation = new SimpleValidation();
        $category = $this->get('category_service')->getCategory($slug);

        $externalLists = $this->getAllExternalListQuestionGroups();
        $simpleValidation->setCategory($category);
        $form = $this->createForm(ExternalListValidation::class, $simpleValidation, [
            'category'      => $category,
            'externalLists' => $externalLists,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->checkFormRequirements($form);

            if ($form->isValid()) {
                $externalListId = $form->get('externalList')->getData();
                $externalList = $this->getDoctrine()->getRepository('AppBundle:QuestionCategory')->find($externalListId);
                $simpleValidation->setExternalList($externalList);
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'External List Validation Rule created!');

                $em = $this->getDoctrine()->getManager();
                $em->persist($simpleValidation);
                $em->flush();

                return $this->redirectToRoute('cms_listvalidation_index', ['slug' => $slug]);
            }
        }


        return $this->render('admin/listvalidation/new.html.twig', [
            'category' => $category,
            'form'     => $form->createView(),
        ]);
    }

    /**
     * Lists all AdvancedValidation entities.
     *
     * @Route("/show/{id}", name="cms_listvalidation_show")
     * @Method("GET")
     * @param int $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function showAction($id)
    {
        /** @var SimpleValidation $simpleValidation */
        $simpleValidation = $this->getDoctrine()->getRepository('AppBundle:SimpleValidation')->find($id);
        $deleteForm = $this->createDeleteForm($simpleValidation);

        if (null === $simpleValidation) {
            throw new NotFoundHttpException('Validation not found');
        }

        return $this->render('admin/listvalidation/show.html.twig', [
            'category'         => $simpleValidation->getCategory(),
            'simpleValidation' => $simpleValidation,
            'delete_form'      => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all AdvancedValidation entities.
     *
     * @Route("/edit/{id}", name="cms_listvalidation_edit")
     * @param Request $request
     * @param int     $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Symfony\Component\Form\Exception\LogicException
     * @throws \Symfony\Component\Form\Exception\AlreadySubmittedException
     * @throws \OutOfBoundsException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function editAction(Request $request, $id)
    {
        /** @var SimpleValidation $simpleValidation */
        $simpleValidation = $this->getDoctrine()->getRepository('AppBundle:SimpleValidation')->find($id);
        $deleteForm = $this->createDeleteForm($simpleValidation);

        if (null === $simpleValidation) {
            throw new NotFoundHttpException('Validation not found');
        }

        $externalLists = $this->getAllExternalListQuestionGroups();
        $form = $this->createForm(ExternalListValidation::class, $simpleValidation, [
            'category'      => $simpleValidation->getCategory(),
            'externalLists' => $externalLists,
        ]);
        $form->get('externalList')->setData($simpleValidation->getExternalList()->getId());
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $this->checkFormRequirements($form);

            if ($form->isValid()) {
                $externalListId = $form->get('externalList')->getData();
                $externalList = $this->getDoctrine()->getRepository('AppBundle:QuestionCategory')->find($externalListId);
                $simpleValidation->setExternalList($externalList);
                $request->getSession()
                    ->getFlashBag()
                    ->add('success', 'External List Validation Rule edited!');

                $em = $this->getDoctrine()->getManager();
                $em->persist($simpleValidation);
                $em->flush();

                return $this->redirectToRoute('cms_listvalidation_index', ['slug' => $simpleValidation->getCategory()->getSlug()]);
            }
        }

        return $this->render('admin/listvalidation/edit.html.twig', [
            'category'         => $simpleValidation->getCategory(),
            'simpleValidation' => $simpleValidation,
            'form'             => $form->createView(),
            'delete_form'      => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a SimpleValidation entity.
     *
     * @Route("/{id}", name="cms_listvalidation_delete")
     * @Method("DELETE")
     * @param Request          $request
     * @param SimpleValidation $simpleValidation
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \LogicException
     */
    public function deleteAction(Request $request, SimpleValidation $simpleValidation)
    {
        $slug = $simpleValidation->getCategory()->getSlug();
        $form = $this->createDeleteForm($simpleValidation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $request->getSession()
                ->getFlashBag()
                ->add('success', 'External List Validation Rule deleted!');

            $em = $this->getDoctrine()->getManager();
            $em->remove($simpleValidation);
            $em->flush();
        }

        return $this->redirectToRoute('cms_listvalidation_index', ['slug' => $slug]);
    }

    /**
     * Creates a form to delete a SimpleValidation entity.
     *
     * @param SimpleValidation $simpleValidation The SimpleValidation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SimpleValidation $simpleValidation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('cms_listvalidation_delete', ['id' => $simpleValidation->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * @param $form
     *
     * @throws \OutOfBoundsException
     */
    private function checkFormRequirements(Form $form)
    {
        $type = $form->get('type')->getData();

        switch ($type) {
            case  ExternalListValidationService::TYPE_TYPE_A_NO:
            case ExternalListValidationService::TYPE_TYPE_A_YES:
                $sourceColumn = $form->get('sourceField')->getData();
                $sourceList = $form->get('sourceList')->getData();
                $compareToColumn = $form->get('compareTo')->getData();
                $externalList = $form->get('externalList')->getData();

                if (!$sourceColumn) {
                    $form->get('sourceField')->addError(new FormError('Please, provide source column name e.g A'));
                }
                if (!$sourceList) {
                    $form->get('sourceList')->addError(new FormError('Please, provide List of source column'));
                }
                if (!$compareToColumn) {
                    $form->get('compareTo')->addError(new FormError('Please, provide column name e.g A for comparison'));
                }
                if (!$externalList) {
                    $form->get('externalList')->addError(new FormError('Please, provide External List of comparison column'));
                }
                break;
            case ExternalListValidationService::TYPE_TYPE_B:
                $sourceColumn = $form->get('sourceField')->getData();
                $sourceColumn2 = $form->get('sourceField2')->getData();
                $sourceList = $form->get('sourceList')->getData();
                $compareToColumn = $form->get('compareTo')->getData();
                $compareToColumn2 = $form->get('compareTo2')->getData();
                $externalList = $form->get('externalList')->getData();

                if (!$sourceColumn) {
                    $form->get('sourceField')->addError(new FormError('Please, provide source column name e.g A - data of column will be used like identificator'));
                }
                if (!$sourceList) {
                    $form->get('sourceList')->addError(new FormError('Please, provide List of source columns'));
                }
                if (!$sourceColumn2) {
                    $form->get('sourceField2')->addError(new FormError('Please, provide source column name e.g B - data of column will be checked'));
                }
                if (!$compareToColumn) {
                    $form->get('compareTo')->addError(new FormError('Please, provide column name e.g A for comparison - data of column will be used like identificator in external list'));
                }
                if (!$compareToColumn2) {
                    $form->get('compareTo2')->addError(new FormError('Please, provide column name e.g B for comparison - data of column will be compared witch checked data'));
                }
                if (!$externalList) {
                    $form->get('externalList')->addError(new FormError('Please, provide External List of comparison columns'));
                }
                break;
            case ExternalListValidationService::TYPE_TYPE_B_CHECK2:
                $sourceColumn = $form->get('sourceField')->getData();
                $sourceColumn2 = $form->get('sourceField2')->getData();
                $sourceList = $form->get('sourceList')->getData();
                $compareToColumn = $form->get('compareTo')->getData();
                $externalList = $form->get('externalList')->getData();

                if (!$sourceColumn) {
                    $form->get('sourceField')->addError(new FormError('Please, provide source column name e.g A - data of column are expected to be empty'));
                }
                if (!$sourceList) {
                    $form->get('sourceList')->addError(new FormError('Please, provide List of source columns'));
                }
                if (!$sourceColumn2) {
                    $form->get('sourceField2')->addError(new FormError('Please, provide source column name e.g B - data of column will be checked'));
                }
                if (!$compareToColumn) {
                    $form->get('compareTo')->addError(new FormError('Please, provide column name e.g A for comparison - data of column will be used for comparison'));
                }
                if (!$externalList) {
                    $form->get('externalList')->addError(new FormError('Please, provide External List of comparison column'));
                }
                break;
        }
    }

    /**
     * @return array
     * @throws \LogicException
     */
    private function getAllExternalListQuestionGroups()
    {
        $externalLists = [];

        $categories = $this->getDoctrine()->getRepository('AppBundle:Category')->findBy([
            'externalList' => true,
        ]);
        if ($categories) {
            foreach ($categories as $cat) {
                $reports = $cat->getReports();
                if ($reports) {
                    /** @var Report $report */
                    foreach ($reports as $report) {
                        $questionCategories = $report->getQuestionCategories();
                        if ($questionCategories) {
                            /** @var QuestionCategory $qc */
                            foreach ($questionCategories as $qc) {
                                $externalLists['#' . $qc->getId() . ' ' . $qc->getName()] = $qc->getId();
                            }
                        }
                    }
                }
            }
        }

        return $externalLists;
    }

}