<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Entity\Category;
use AppBundle\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ProjectCategoryController
 * @Route("/cms/project-category")
 *
 * @package AppBundle\Controller\Admin
 */
class ProjectCategoryController extends Controller
{
    /**
     * @Route("/new", name="admin_cms_project_category_new")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \InvalidArgumentException
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function newProjectCategoryAction(Request $request)
    {
        $category = new Category();

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($category);
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success' , 'New project created!');

            return $this->redirectToRoute('admin_cms_index');
        }

        return $this->render('admin/projectCategory/new.html.twig' , [
            'category' => $category ,
            'form'     => $form->createView() ,
        ]);
    }

    /**
     * @Route("/edit/{id}", name="admin_cms_project_category_edit")
     *
     * @param  int    $id
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function editProjectCategoryAction($id, Request $request)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);

        if(null === $category)
        {
            throw new NotFoundHttpException('Category not found!');
        }

        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($category);
            $this->getDoctrine()->getManager()->flush();
            $request->getSession()
                ->getFlashBag()
                ->add('success' , 'Project updated!');

            return $this->redirectToRoute('admin_cms_index');
        }

        return $this->render('admin/projectCategory/edit.html.twig' , [
            'category_id'   => $id ,
            'form'          => $form->createView() ,
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_cms_project_category_delete", requirements={"id"="\d+"})
     * @param int     $id
     *
     * @param Request $request
     *
     * @return array
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function deleteAction($id , Request $request)
    {
        $category = $this->getDoctrine()->getRepository('AppBundle:Category')->find($id);

        if (null === $category) {
            throw new NotFoundHttpException('Category not found');
        }
        $request->getSession()
            ->getFlashBag()
            ->add('success' , 'Project deleted!');
        $this->getDoctrine()->getManager()->remove($category);
        $this->getDoctrine()->getManager()->flush();

        return $this->redirectToRoute('admin_cms_index');
    }
}
