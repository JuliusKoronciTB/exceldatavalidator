<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Form\ExcelType;
use AppBundle\Entity\Excel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class DefaultController
 * @Route("/cms")
 *
 * @package AppBundle\Controller\Admin
 */
class AdminController extends Controller
{
    /**
     * @Route("", name="admin_cms_index")
     * @throws \LogicException
     * @throws \UnexpectedValueException
     */
    public function indexAction()
    {
        return $this->render('admin/admin/home.html.twig' , [
            'categories' => $this->getDoctrine()->getRepository('AppBundle:Category')->findAll(),
        ]);
    }
}
