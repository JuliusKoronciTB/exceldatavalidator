<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ExcelService
 *
 * @package AppBundle\Services
 */
class CategoryService
{
    /** @var EntityManager */
    private $em;

    /**
     * ExcelDataService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return \AppBundle\Entity\Category[]|array
     */
    public function getAllCategories()
    {
        return $this->em->getRepository('AppBundle:Category')->findAll();
    }

    /**
     * @param string $slug
     *
     * @return \AppBundle\Entity\Category|null
     * @throws \LogicException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function getCategory($slug)
    {
        $category = $this->em->getRepository('AppBundle:Category')->findOneBy([
            'slug' => $slug,
        ]);

        if (null === $category) {
            throw new NotFoundHttpException('Category not found');
        }

        return $category;
    }

    /**
     * @param string $slug
     * @param bool   $count
     *
     * @return array|int
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function getAllValidationsRules($slug, $count = false)

    {
        $category = $this->getCategory($slug);

        $simpleValidationsRules = $category->getSimpleValidations();
        $advancedValidationsRules = $category->getAdvancedValidations();

        $allValidationsRules = array_merge($simpleValidationsRules->toArray(), $advancedValidationsRules->toArray());

        if ($count) {
            return count($allValidationsRules);
        }

        return $allValidationsRules;
    }

    /**
     * @param string  $slug
     * @param boolean $autoRun
     *
     * @return \AppBundle\Entity\SimpleValidation[]|array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function getAutoRunSimpleValidationRules($slug, $autoRun)
    {
        $category = $this->getCategory($slug);

        return $this->em->getRepository('AppBundle:SimpleValidation')->getAutoRunSimpleValidationRules($category, $autoRun);
    }

    /**
     * @param string   $slug
     *
     * @param boolean $autoRun
     *
     * @return \AppBundle\Entity\SimpleValidation[]|array
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \LogicException
     */
    public function getExternalListSimpleValidationRules($slug, $autoRun)
    {
        $category = $this->getCategory($slug);

        return $this->em->getRepository('AppBundle:SimpleValidation')->getAutoRunExternalListSimpleValidationRules($category, $autoRun);
    }

}