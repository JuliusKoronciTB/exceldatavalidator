<?php

namespace AppBundle\Services;

use AppBundle\Entity\Category;
use Doctrine\ORM\EntityManager;

/**
 * Class ExcelService
 *
 * @package AppBundle\Services
 */
class ExcelService
{
    /** @var EntityManager */
    private $em;

    /**
     * ExcelDataService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param Category $category
     * @param bool     $count
     *
     * @return array|int
     */
    public function getAllValidatedExcelsForCategory(Category $category , $count = false)
    {
        $excels = $this->em->getRepository('AppBundle:Excel')->getAllValidatedExcelsForCategory($category);

        if ($count) {
            return count($excels);
        }

        return $excels;
    }

    /**
     * @param Category $category
     * @param bool     $count
     *
     * @return array|int
     */
    public function getAllProcessedExcelsForCategory(Category $category , $count = false)
    {
        $excels = $this->em->getRepository('AppBundle:Excel')->getAllProcessedExcelsForCategory($category);

        if ($count) {
            return count($excels);
        }

        return $excels;
    }
}