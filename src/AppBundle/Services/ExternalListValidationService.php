<?php

namespace AppBundle\Services;

use AppBundle\Entity\Excel;
use AppBundle\Entity\ExcelData;
use AppBundle\Entity\SimpleValidation;
use Doctrine\ORM\EntityManager;

/**
 * Class ExternalListValidationService
 *
 * @package AppBundle\Services
 */
class ExternalListValidationService
{
    const TYPE_TYPE_A_YES = 'Type_A:_source_column_data_are_in_external_list_column';
    const TYPE_TYPE_A_NO = 'Type_A:_at_least_one_source_column_data_is_not_in_external_list_column';
    const TYPE_TYPE_B = 'Type_B_check1:_compare_data_based_on_selected_column';
    const TYPE_TYPE_B_CHECK2 = 'Type_B_check2:_find_missing_data';

    /** @var  ExcelDataService */
    private $excelDataService;

    /** @var EntityManager */
    private $em;

    public function __construct(ExcelDataService $excelDataService, EntityManager $em)
    {
        $this->excelDataService = $excelDataService;
        $this->em = $em;
    }

    /**
     * @param Excel $excel
     *
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     */
    public function validate(Excel $excel)
    {
        $logs = [
            'errors' => [],
            'logs'   => [],
        ];
        $category = $excel->getCategory();
        /** @var SimpleValidation $validation */
        foreach ($this->excelDataService->getExternalListSimpleValidationRules($category) as $validation) {
            if (false === $validation->getAutoRun()) {
                continue;
            }
            $return = $this->validateByRule($excel, $validation);
            if ($return['type'] === 'error') {
                $logs['errors'][] = $return['value'];
            } else {
                $logs['logs'][] = $return['value'];
            }
        }

        return $logs;
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return array
     */
    public function validateByRule(Excel $excel, SimpleValidation $validationRule)
    {
        $error = $validationRule->getDisplayReason();
        $log = $validationRule->getDisplayCode();
        try {
            switch ($validationRule->getType()) {
                case self::TYPE_TYPE_A_YES:
                    $notFoundRecords = $this->typeAValidation($excel, $validationRule);
                    if (count($notFoundRecords) > 0) {
                        $error .= ' Check following fields: ';
                        /** @var ExcelData $record */
                        foreach ($notFoundRecords as $record) {
                            $error .= $record->getCol() . $record->getRow() . ' ' . $record->getValue() . ',';
                        }

                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $log];
                    break;
                case self::TYPE_TYPE_A_NO:
                    $notFoundRecords = $this->typeAValidation($excel, $validationRule);
                    if (count($notFoundRecords) > 0) {
                        return ['type' => 'log', 'value' => $log];

                    }

                    return ['type' => 'error', 'value' => $error];
                    break;
                case self::TYPE_TYPE_B:
                    $return = $this->typeBValidation($excel, $validationRule);

                    if (count($return['errors']) === 0 && count($return['dataWithoutSourceInExternalList']) === 0 && count($return['dataWithAnotherProblem']) === 0) {
                        return ['type' => 'log', 'value' => $log];
                    }

                    if (count($return['errors']) > 0) {
                        $error .= ' Following values were not found in external list: ';
                        /** @var ExcelData $record */
                        foreach ($return['errors'] as $key => $value) {
                            $error .= $value . ' ,';
                        }
                    }
                    if (count($return['dataWithoutSourceInExternalList']) > 0) {
                        $error .= ' Following values have not source in External list or are duplicated: ';
                        /** @var ExcelData $record */
                        foreach ($return['dataWithoutSourceInExternalList'] as $key => $value) {
                            $error .= $value . ' ,';
                        }
                    }
                    if (count($return['dataWithAnotherProblem']) > 0) {
                        $error .= ' Following value has problem: ';
                        /** @var ExcelData $record */
                        foreach ($return['dataWithAnotherProblem'] as $key => $value) {
                            $error .= $value . ' ,';
                        }
                    }

                    return ['type' => 'error', 'value' => $error];
                    break;
                case self::TYPE_TYPE_B_CHECK2:
                    $return = $this->typeBCheck2Validation($excel, $validationRule);

                    if (count($return) === 0) {
                        return ['type' => 'log', 'value' => $log];
                    }

                    if (count($return) > 0) {
                        $error .= ' You should check following data: ';
                        /** @var ExcelData $record */
                        foreach ($return as $key => $value) {
                            $error .= $value . ' ,';
                        }
                    }

                    return ['type' => 'error', 'value' => $error];
                    break;
            }
        } catch (\Exception $e) {
            return ['type' => 'error', 'value' => $error];
        }
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return array
     */
    private function typeAValidation(Excel $excel, SimpleValidation $validationRule)
    {
        return $this->excelDataService->findListDataInList($excel, $validationRule->getSourceList(), $validationRule->getSourceField(), $validationRule->getExternalList(), $validationRule->getCompareTo());
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return array
     */
    private function typeBValidation(Excel $excel, SimpleValidation $validationRule)
    {
        $sourceData = [
            'sourceList' => $validationRule->getSourceList(),
            'sourceCol1' => $validationRule->getSourceField(),
            'sourceCol2' => $validationRule->getSourceField2(),
        ];
        $compareData = [
            'compareList' => $validationRule->getExternalList(),
            'compareCol1' => $validationRule->getCompareTo(),
            'compareCol2' => $validationRule->getCompareTo2(),
        ];

        return $this->excelDataService->compareDataBasedOnColumn($excel, $sourceData, $compareData);
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return array
     */
    private function typeBCheck2Validation(Excel $excel, SimpleValidation $validationRule)
    {
        $sourceData = [
            'sourceList' => $validationRule->getSourceList(),
            'sourceCol1' => $validationRule->getSourceField(),
            'sourceCol2' => $validationRule->getSourceField2(),
        ];
        $compareData = [
            'compareList' => $validationRule->getExternalList(),
            'compareCol1' => $validationRule->getCompareTo(),
        ];

        return $this->excelDataService->findMissingData($excel, $sourceData, $compareData);
    }

    /**
     * @return array
     */
    public static function getConstants()
    {
        $oClass = new \ReflectionClass(__CLASS__);

        return $oClass->getConstants();
    }

}