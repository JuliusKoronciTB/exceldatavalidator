<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 9/10/16
 * Time: 9:00 AM
 */

namespace AppBundle\Services;


use AppBundle\Entity\QuestionCategory;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

class MappingService
{
    const CACHE_PREFIX = 'map_to_category_';

    public function destroyMap($categoryId)
    {
        $cache = new FilesystemAdapter();

        $cache->deleteItem(self::CACHE_PREFIX . $categoryId);
    }

    public function createMap($reports)
    {
        $map = [];
        foreach ($reports as $report) {
            $categories = $report->getQuestionCategories();
            foreach ($categories as $categry) {
                if (!empty($categry->getRows())) {
                    $rows = explode(',' , $categry->getRows());
                    $this->handleRows($categry , $rows , $map);
                } elseif (null !== $categry->getRowEnd()) {
                    $this->handleStartEndRows($categry , $map);
                } else {
                    $this->handleStartNoEndRows($categry , $map);
                }
            }
        }

        return $map;
    }

    public function handleRows($cat , $rows , &$map)
    {
        foreach ($rows as $row) {
            $this->addToMap($cat , $row , $map);
        }
    }

    public function handleStartEndRows($cat , &$map)
    {
        $count = 0;
        $start = $cat->getRowStart();
        $limit = $cat->getRowEnd() - $start + 1;

        while ($count < $limit) {
            $this->addToMap($cat , $start , $map);
            $start++;
            $count++;
            if ($count > 3000) {
                break;
            }
        }
    }

    public function handleStartNoEndRows($cat , &$map)
    {
        $count = 0;
        $start = $cat->getRowStart();
        $limit = 3000;

        while ($count < $limit) {
            $this->addToMap($cat , $start , $map);
            $start++;
            $count++;
        }
    }

    public function addToMap(QuestionCategory $cat , $row , &$map)
    {
        $sheetName = $cat->getReport()->getSheetName();
        $columns = $cat->getColumns();
        foreach ($columns as $column) {

            $map[$sheetName][] = strtoupper($column->getCol() . $row);
        }
        unset($columns , $row);
    }
}