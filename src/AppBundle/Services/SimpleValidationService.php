<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 8/29/16
 * Time: 4:34 PM
 */

namespace AppBundle\Services;

use AppBundle\Entity\Excel;
use AppBundle\Entity\ExcelData;
use AppBundle\Entity\QuestionCategory;
use AppBundle\Entity\SimpleValidation;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class SimpleValidationService
 *
 * @package AppBundle\Services
 */
class SimpleValidationService implements ValidationInterface
{
    const TYPE_FILE_EXTENSION = 'file_extension';
    const TYPE_IS_BLANK = 'field_is_blank';
    const TYPE_NOT_BLANK = 'field_is_not_blank';
    const TYPE_EQUALS_TO_FIELD = 'equal_to_cell';
    const TYPE_DOES_NOT_EQUALS_TO_FIELD = 'not_equal_to_cell';
    const TYPE_EQUALS_TO_VALUE = 'equal_to_value';
    const TYPE_DOES_NOT_EQUALS_TO_VALUE = 'not_equal_to_value';
    const TYPE_IN_LIST = 'value_in_list_of_values';
    const TYPE_CONTAINS_SYMBOL = 'contains_symbol';
    const TYPE_IS_EMAIL = 'is_email';
    const TYPE_IS_DATE = 'is_date';
    const TYPE_IS_DATE_IN_FORMAT_DD_MM_YYYY = 'is_date_in_specific_format_DD_MM_YYYY';
    const TYPE_IS_DATE_IN_FUTURE = 'is_date_in_future';
    const TYPE_IS_DATE_IN_PAST = 'is_date_in_past';
    const TYPE_IS_DATE_OLDER_THAN_MONTHS = 'is_date_older_in_months';
    const TYPE_IS_DATE_NEWER_THAN_MONTHS = 'is_date_newer_in_months';
    const TYPE_LIST_IS_BLANK = 'list_is_blank';
    const TYPE_LIST_NOT_BLANK = 'list_is_not_blank';
    const TYPE_LIST_GREATER_THAN = 'list_is_greater_than';

    /** @var  ExcelDataService */
    private $excelDataService;

    /** @var EntityManager */
    private $em;

    public function __construct(ExcelDataService $excelDataService, EntityManager $em)
    {
        $this->excelDataService = $excelDataService;
        $this->em = $em;
    }

    /**
     * @param Excel $excel
     *
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     */
    public function validate(Excel $excel)
    {
        $logs = [
            'errors' => [],
            'logs'   => [],
        ];
        $category = $excel->getCategory();
        /** @var SimpleValidation $validation */
        foreach ($this->excelDataService->getActiveRulesForSimpleValidation($category) as $validation) {
            if (false === $validation->getAutoRun()) {
                continue;
            }
            $return = $this->validateByRule($excel, $validation);
            if ($return['type'] === 'error') {
                $logs['errors'][] = $return['value'];
            } else {
                $logs['logs'][] = $return['value'];
            }
        }

        return $logs;
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     */
    public function validateByRule(Excel $excel, SimpleValidation $validationRule)
    {
        $error = $validationRule->getDisplayCode() . ' ' . $validationRule->getDisplayReason() . ' ' . $validationRule->getDisplayMessage();
        try {
            switch ($validationRule->getType()) {
                case self::TYPE_FILE_EXTENSION:
                    if (false === $this->validateFileExtension($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IS_BLANK:
                    if (false === $this->fieldIsBlank($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_DOES_NOT_EQUALS_TO_FIELD:
                    if (false === $this->notEqualsToField($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_EQUALS_TO_FIELD:
                    if (false === $this->equalsToField($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_DOES_NOT_EQUALS_TO_VALUE:
                    if (false === $this->doesNotEqualsToValue($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_EQUALS_TO_VALUE:
                    if (false === $this->equalsToValue($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IN_LIST:
                    if (false === $this->valueIsInList($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_NOT_BLANK:
                    if (false === $this->notBlank($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_CONTAINS_SYMBOL:
                    if (false === $this->containsSymbol($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IS_EMAIL:
                    if (false === $this->isEmail($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IS_DATE:
                    if (false === $this->isDate($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IS_DATE_IN_FORMAT_DD_MM_YYYY:
                    if (false === $this->isDateInSpecificFormat($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IS_DATE_IN_FUTURE:
                    if (false === $this->isDateInFuture($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IS_DATE_IN_PAST:
                    if (false === $this->isDateInPast($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IS_DATE_OLDER_THAN_MONTHS:
                    if (false === $this->isDateOlderThanMonths($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_IS_DATE_NEWER_THAN_MONTHS:
                    if (false === $this->isDateNewerThanMonths($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_LIST_IS_BLANK:
                    if (false === $this->listIsBlank($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_LIST_NOT_BLANK:
                    if (false === $this->listIsNotBlank($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                case self::TYPE_LIST_GREATER_THAN:
                    if (false === $this->listGreaterThan($excel, $validationRule)) {
                        return ['type' => 'error', 'value' => $error];
                    }

                    return ['type' => 'log', 'value' => $error];
                    break;
                default:
                    return ['type' => 'error', 'value' => 'Unexpected error - please check your validation rules'];
                    break;
            }
        } catch (\Exception $e) {
            return ['type' => 'error', 'value' => $error];
        }
    }

    /**
     * @param Excel            $excel
     *
     * @param SimpleValidation $validationRule
     *
     * @return bool
     */
    private function validateFileExtension(Excel $excel, SimpleValidation $validationRule)
    {
        $compareTo = str_replace(' ', '', $validationRule->getCompareTo());
        $compareTo = strtolower($compareTo);

        $excelFileName = $excel->getFilename();
        $indexOfExtensionBegin = strpos($excelFileName, '.');

        return substr($excelFileName, ++$indexOfExtensionBegin) === $compareTo;
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     */
    private function notBlank($excel, $validationRule)
    {
        $value = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        return !empty($value);
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     */
    private function fieldIsBlank($excel, $validationRule)
    {
        $value = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        if (empty($value)) {
            return true;
        }

        return false;
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     */
    private function equalsToValue(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());
        $string = str_replace(' ', '', preg_replace('/\s+/', '', trim($value)));
        $compare = str_replace(' ', '', $validationRule->getCompareTo());

        return $string === $compare;
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     */
    private function doesNotEqualsToValue(Excel $excel, SimpleValidation $validationRule)
    {
        return !$this->equalsToValue($excel, $validationRule);
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     */
    private function equalsToField(Excel $excel, SimpleValidation $validationRule)
    {
        $sourceValue = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());
        $compareValue = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getCompareToSheet(), $validationRule->getCategory()->getId(), $validationRule->getCompareTo());

        $stringSourceValue = str_replace(' ', '', preg_replace('/\s+/', '', trim($sourceValue)));
        $stringCompareValue = str_replace(' ', '', preg_replace('/\s+/', '', trim($compareValue)));

        return $stringSourceValue === $stringCompareValue;
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     */
    private function notEqualsToField(Excel $excel, SimpleValidation $validationRule)
    {
        return !$this->equalsToField($excel, $validationRule);
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool|int
     * @throws \Doctrine\ORM\NoResultException
     */
    private function valueIsInList(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());
        $list = $validationRule->getCompareTo();

        $stringSourceValue = str_replace(' ', '', preg_replace('/\s+/', '', trim($value)));
        $stringSourceValue = strtolower($stringSourceValue);

        $stringList = strtolower($list);

        if (strpos($stringList, $stringSourceValue)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     */
    private function containsSymbol(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());
        $symbol = $validationRule->getCompareTo();

        if (strpos($value, $symbol)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     */
    private function isEmail(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     */
    private function isDate(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        return $this->checkDate($value);
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     */
    private function isDateInFuture(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getDateValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        if (!empty($value)) {
            $today = new \DateTime();

            $valueDate = new \DateTime($value);

            return $today < $valueDate;
        } else {
            return false;
        }

    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     */
    private function isDateInPast(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getDateValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        if (!empty($value)) {
            $today = new \DateTime();

            $valueDate = new \DateTime($value);

            return $today > $valueDate;
        } else {
            return false;
        }
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     */
    private function isDateOlderThanMonths(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getDateValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        $numberOfMonths = $validationRule->getCompareTo();

        if ($numberOfMonths) {

            if (!empty($value)) {
                $today = new \DateTime();
                try {
                    $todayModify = $today->modify('-' . $numberOfMonths . ' month');

                    $valueDate = new \DateTime($value);

                    return $todayModify >= $valueDate;
                } catch (Exception $e) {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     */
    private function isDateNewerThanMonths(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getDateValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        $numberOfMonths = $validationRule->getCompareTo();

        if ($numberOfMonths) {

            if (!empty($value)) {
                $today = new \DateTime();
                try {
                    $todayModify = $today->modify('-' . $numberOfMonths . ' month');

                    $valueDate = new \DateTime($value);

                    return $todayModify <= $valueDate;
                } catch (Exception $e) {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     */
    private function isDateInSpecificFormat(Excel $excel, SimpleValidation $validationRule)
    {
        $value = $this->excelDataService->getDateValueForValidationRule($excel, $validationRule->getSourceSheet(), $validationRule->getCategory()->getId(), $validationRule->getSourceField());

        if (preg_match('/\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}/', $value) || preg_match('/\d{2}-\d{2}-\d{4} \d{2}:\d{2}:\d{2}/', $value)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NoResultException
     */
    private function listIsBlank(Excel $excel, SimpleValidation $validationRule)
    {
        $listId = $validationRule->getSourceList();

        $questionCategoryGroup = $this->em->getRepository('AppBundle:QuestionCategory')->find($listId);

        if (null === $questionCategoryGroup) {
            throw new NotFoundHttpException('Question category not found');
        } else {
            $numberOfNotEmptyValues = $this->em->getRepository('AppBundle:ExcelData')->getNumberOfNotEmptyValuesInQuestionCategory($excel, $questionCategoryGroup);
            if (0 === (int)$numberOfNotEmptyValues) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return bool
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NoResultException
     */
    private function listIsNotBlank(Excel $excel, SimpleValidation $validationRule)
    {
        $isBlank = $this->listIsBlank($excel, $validationRule);

        return !$isBlank;
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validationRule
     *
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @throws \Doctrine\ORM\NoResultException
     */
    private function listGreaterThan(Excel $excel, SimpleValidation $validationRule)
    {
        $listId = $validationRule->getSourceList();

        /** @var QuestionCategory $questionCategoryGroup */
        $questionCategoryGroup = $this->em->getRepository('AppBundle:QuestionCategory')->find($listId);
        $greaterThanValue = $validationRule->getCompareTo();

        if (null === $questionCategoryGroup) {
            throw new NotFoundHttpException('Question category not found');
        } else {
            $notEmptyValues = $this->em->getRepository('AppBundle:ExcelData')->getNumberOfNotEmptyValuesInQuestionCategory($excel, $questionCategoryGroup);

            $numberOfNotEmptyValues = count($notEmptyValues);
            $numberofRows = 0;
            $row = 0;
            if ($numberOfNotEmptyValues > 0) {
                /** @var ExcelData $val */
                foreach ($notEmptyValues as $val) {
                    if ($row !== $val->getRow()) {
                        $numberofRows++;
                        $row = $val->getRow();
                    }
                }

                if ($greaterThanValue < $numberofRows) {
                    return true;
                }
            }

        }

        return false;
    }

    /**
     * @param string $value
     *
     * @return bool
     */
    private function checkDate($value)
    {
        $date = date_parse($value);

        if ($date["error_count"] === 0 && checkdate($date["month"], $date["day"], $date["year"])) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return array
     */
    public static function getConstants()
    {
        $oClass = new \ReflectionClass(__CLASS__);

        return $oClass->getConstants();
    }
}