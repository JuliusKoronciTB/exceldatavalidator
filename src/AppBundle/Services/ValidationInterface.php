<?php
namespace AppBundle\Services;

use AppBundle\Entity\Excel;

interface ValidationInterface
{
    public function validate(Excel $excel);
}