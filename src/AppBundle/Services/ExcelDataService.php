<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 8/28/16
 * Time: 12:00 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\Category;
use AppBundle\Entity\Excel;
use AppBundle\Entity\ExcelData;
use AppBundle\Entity\QuestionCategory;
use AppBundle\Entity\Report;
use AppBundle\Entity\SimpleValidation;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExcelDataService
{
    /** @var EntityManager */
    private $em;

    /**
     * ExcelDataService constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param QuestionCategory $questionCategory
     * @param Excel            $excel
     *
     * @return string
     */
    public function getRowsForLoop(QuestionCategory $questionCategory, Excel $excel)
    {
        return $this->em->getRepository('AppBundle:ExcelData')->getAllRowsForCategory($questionCategory, $excel);
    }

    /**
     * @param Excel            $excel
     * @param QuestionCategory $questionCategory
     * @param                  $row
     * @param                  $column
     *
     * @return string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getValueForColumnRow(Excel $excel, QuestionCategory $questionCategory, $row, $column)
    {
        try {
            return $this->em->getRepository('AppBundle:ExcelData')->getValueForColumnRow($excel, $questionCategory, $row, $column);
        } catch (\Exception $e) {
            return '';
        }
    }

    /**
     * @param Excel  $excel
     * @param string $sheetName
     * @param int    $categoryId
     * @param string $coordinate
     *
     * @return null|string
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getValueForValidationRule(Excel $excel, $sheetName, $categoryId, $coordinate)
    {
        $report = $this->em->getRepository('AppBundle:Report')->findOneBy([
            'sheetName' => $sheetName,
            'category'  => $categoryId,
        ]);

        return $this->em->getRepository('AppBundle:ExcelData')->getValueForValidationRule($excel, $report, $coordinate);
    }

    /**
     * @param Excel  $excel
     * @param string $sheetName
     * @param int    $categoryId
     * @param string $coordinate
     *
     * @return null|string
     * @throws \Doctrine\ORM\NoResultException
     */
    public function getDateValueForValidationRule(Excel $excel, $sheetName, $categoryId, $coordinate)
    {
        $report = $this->em->getRepository('AppBundle:Report')->findOneBy([
            'sheetName' => $sheetName,
            'category'  => $categoryId,
        ]);

        return $this->em->getRepository('AppBundle:ExcelData')->getDateValueForValidationRule($excel, $report, $coordinate);
    }

    /**
     * @param Excel            $excel
     * @param int              $sourceQuestionCategoryId
     * @param string           $sourceColumn
     * @param QuestionCategory $externalListQuestionCategory
     * @param string           $externalListColumn
     *
     * @return array
     */
    public function findListDataInList(Excel $excel, $sourceQuestionCategoryId, $sourceColumn, QuestionCategory $externalListQuestionCategory, $externalListColumn)
    {
        $actualExternalExcel = $this->getActualExternalList($externalListQuestionCategory);

        $notFoundRecords = $this->em->getRepository('AppBundle:ExcelData')->findListDataInList($excel, $sourceQuestionCategoryId, $sourceColumn, $actualExternalExcel, $externalListQuestionCategory, $externalListColumn);

        return $notFoundRecords;
    }

    /**
     * @param Excel $excel
     * @param array $sourceData
     * @param array $compareData
     *
     * @return array
     */
    public function compareDataBasedOnColumn(Excel $excel, $sourceData, $compareData)
    {
        $actualExternalExcel = $this->getActualExternalList($compareData['compareList']);

        $sourceQuestionCategoryId = $sourceData['sourceList'];
        $sourceColumn1 = $sourceData['sourceCol1'];
        $sourceColumn2 = $sourceData['sourceCol2'];
        $externalListColumn1 = $compareData['compareCol1'];
        $externalListColumn2 = $compareData['compareCol2'];

        $source1Data = $this->em->getRepository('AppBundle:ExcelData')->findBy([
            'excel'            => $excel,
            'questionCategory' => $sourceQuestionCategoryId,
            'col'              => $sourceColumn1,
        ]);

        $dataWithoutSourceInExternalList = [];
        $errors = [];
        $dataWithAnotherProblem = [];

        /** @var ExcelData $data */
        foreach ($source1Data as $data) {
            $mainValue = $data->getValue();
            if (!empty($mainValue)) {
                $externalListMainValue = $this->em->getRepository('AppBundle:ExcelData')->findOneBy([
                    'excel' => $actualExternalExcel,
                    'value' => $mainValue,
                    'col'   => $externalListColumn1,
                ]);

                if (count($externalListMainValue) === 1) {
                    $rowExternalList = $externalListMainValue->getRow();
                    $externalListCompareValue = $this->em->getRepository('AppBundle:ExcelData')->findOneBy([
                        'excel' => $actualExternalExcel,
                        'row'   => $rowExternalList,
                        'col'   => $externalListColumn2,
                    ]);

                    $row = $data->getRow();
                    $sourceColumn2Value = $this->em->getRepository('AppBundle:ExcelData')->findOneBy([
                        'excel'            => $excel,
                        'questionCategory' => $sourceQuestionCategoryId,
                        'col'              => $sourceColumn2,
                        'row'              => $row,
                    ]);

                    if ($externalListCompareValue && $sourceColumn2Value) {
                        if ($externalListCompareValue->getValue() !== $sourceColumn2Value->getValue()) {
                            $errors[] = $mainValue;
                        }
                    } else {
                        $dataWithAnotherProblem[] = $mainValue;
                    }
                } else {
                    $dataWithoutSourceInExternalList[] = $mainValue;
                }
            }
        }

        return [
            'dataWithoutSourceInExternalList' => $dataWithoutSourceInExternalList,
            'errors'                          => $errors,
            'dataWithAnotherProblem'          => $dataWithAnotherProblem,
        ];
    }

    /**
     * @param Excel $excel
     * @param array $sourceData
     * @param array $compareData
     *
     * @return array
     */
    public function findMissingData(Excel $excel, $sourceData, $compareData)
    {
        $actualExternalExcel = $this->getActualExternalList($compareData['compareList']);

        $sourceQuestionCategory = $this->em->getRepository('AppBundle:QuestionCategory')->find($sourceData['sourceList']);
        $sourceColumnEmpty = $sourceData['sourceCol1'];
        $sourceColumnNotEmpty = $sourceData['sourceCol2'];
        $externalListColumn1 = $compareData['compareCol1'];

        $dataWithEmptySC1 = $this->em->getRepository('AppBundle:ExcelData')->getDataWithEmptySource($excel, $sourceData['sourceList'], $sourceColumnEmpty);

        $errors = [];

        if ($dataWithEmptySC1) {
            foreach ($dataWithEmptySC1 as $data) {
                $row = $data->getRow();
                $dataWithNotEmptySC2 = $this->em->getRepository('AppBundle:ExcelData')->getValueForColumnRow($excel, $sourceQuestionCategory, $row, $sourceColumnNotEmpty);
                if (!empty($dataWithNotEmptySC2)) {
                    $ret = $this->em->getRepository('AppBundle:ExcelData')->getEntityBasedOnItsValue($actualExternalExcel, $externalListColumn1, $dataWithNotEmptySC2);
                    if ($ret) {
                        $errors[] = $dataWithNotEmptySC2;
                    }
                }
            }
        }

        return $errors;
    }

    /**
     * @param Category $category
     *
     * @return \AppBundle\Entity\SimpleValidation[]|array
     */
    public function getActiveRulesForSimpleValidation(Category $category)
    {
        return $this->em->getRepository('AppBundle:SimpleValidation')->getAutoRunSimpleValidationRules($category, true);
    }

    /**
     * @param Category $category
     *
     * @return \AppBundle\Entity\SimpleValidation[]|array
     */
    public function getExternalListSimpleValidationRules(Category $category)
    {
        return $this->em->getRepository('AppBundle:SimpleValidation')->getAutoRunExternalListSimpleValidationRules($category, true);
    }

    /**
     * @param QuestionCategory $externalListQuestionCategory
     *
     * @return
     */
    private function getActualExternalList(QuestionCategory $externalListQuestionCategory)
    {
        $externalListCategory = $externalListQuestionCategory->getReport()->getCategory();

        return $this->em->getRepository('AppBundle:Excel')->findOneBy([
            'category' => $externalListCategory,
        ], [
            'createdAt' => 'desc',
        ], 1);
    }
}