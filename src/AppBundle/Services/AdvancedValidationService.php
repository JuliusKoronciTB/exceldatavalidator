<?php
/**
 * Created by PhpStorm.
 * User: websolutions
 * Date: 9/2/16
 * Time: 12:47 PM
 */

namespace AppBundle\Services;

use AppBundle\Entity\AdvancedValidation;
use AppBundle\Entity\Excel;
use AppBundle\Entity\SimpleValidation;

/**
 * Class AdvancedValidationService
 *
 * @package AppBundle\Services
 */
class AdvancedValidationService implements ValidationInterface
{
    /** @var  SimpleValidationService */
    private $simplevalidationService;

    public function __construct(SimpleValidationService $sv)
    {
        $this->simplevalidationService = $sv;
    }

    public function validate(Excel $excel)
    {
        $logs = [
            'errors' => [] ,
            'logs'   => [] ,
        ];
        $category = $excel->getCategory();
        foreach ($category->getAdvancedValidations() as $validation) {
            $return = $this->validateByRule($excel , $validation);
            if ($return['type'] === 'error') {
                $logs['errors'][] = $return['value'];
            } else {
                $logs['logs'][] = $return['value'];
            }
        }

        return $logs;
    }

    /**
     * @param Excel              $excel
     * @param AdvancedValidation $validation
     *
     * @return array
     * @throws \Exception
     */
    public function validateByRule(Excel $excel , AdvancedValidation $validation)
    {
        $error = $validation->getDisplayCode() . ' ' . $validation->getDisplayReason() . ' ' . $validation->getDisplayMessage();
        if (true === $this->getAdvancedValidationResult($excel , $validation)) {
            return ['type' => 'error' , 'value' => $error];
        }

        return ['type' => 'log' , 'value' => $error];
    }

    /**
     * @param Excel            $excel
     * @param SimpleValidation $validation
     *
     * @return bool
     * @throws \Doctrine\ORM\NoResultException
     */
    private function getSimpleValidationResult(Excel $excel , SimpleValidation $validation)
    {
        $return = $this->simplevalidationService->validateByRule($excel , $validation);

        return !('error' === $return['type']);

    }

    /**
     * @param Excel              $excel
     * @param AdvancedValidation $validation
     *
     * @return bool
     * @throws \Exception
     */
    private function getAdvancedValidationResult(Excel $excel , AdvancedValidation $validation)
    {
        $results = [];
        foreach ($validation->getSimpleValidations() as $simpleValidation) {
            $results[] = $this->getSimpleValidationResult($excel , $simpleValidation);
        }

        foreach ($validation->getChildrenAdvancedValidations() as $childrenAdvancedValidation) {
            $results[] = $this->getAdvancedValidationResult($excel , $childrenAdvancedValidation);
        }

        $logic = $validation->getLogic();
        if ($logic === AdvancedValidation::LOGIC_AND) {
            return $this->isAnd($results);
        }
        if ($logic === AdvancedValidation::LOGIC_OR) {
            return $this->isOr($results);
        }

        throw new \Exception('Logic not implemented');
    }

    /**
     * @param array $results
     *
     * @return bool
     */
    private function isAnd(array $results)
    {
        foreach ($results as $result) {
            if (false === $result) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $results
     *
     * @return bool
     */
    private function isOr(array $results)
    {
        foreach ($results as $result) {
            if (true === $result) {
                return true;
            }
        }

        return false;
    }
}