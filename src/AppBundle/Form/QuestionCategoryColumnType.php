<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class QuestionCategoryColumnType
 *
 * @package AppBundle\Form
 */
class QuestionCategoryColumnType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $builder
            ->add('name' , null , [
                'label' => 'Title',
            ])
            ->add('col' , null , [
                'label' => 'Column',
            ])
            ->add('isDate' , null , [
                'label' => 'Expected to be date',
            ]);
    }
}
