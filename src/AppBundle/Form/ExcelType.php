<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;

/**
 * Class ExcelType
 *
 * @package AppBundle\Form
 */
class ExcelType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $builder
            ->add('file' , FileType::class , [
                'mapped'      => false ,
                'required'    => false ,
                'constraints' => new File([
                    'maxSize'          => '10M' ,
                    'mimeTypes'        => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' ,
                    'mimeTypesMessage' => 'Please upload your report in xlsx format' ,
                ]) ,
            ]);
    }
}
