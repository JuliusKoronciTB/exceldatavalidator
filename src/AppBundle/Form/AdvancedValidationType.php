<?php

namespace AppBundle\Form;

use AppBundle\Entity\AdvancedValidation;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdvancedValidationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        $builder
            ->add('name')
            ->add('description')
            ->add('logic' , ChoiceType::class , [
                'choices' => [
                    'AND' => AdvancedValidation::LOGIC_AND ,
                    'OR'  => AdvancedValidation::LOGIC_OR ,
                ] ,
            ])
            ->add('simpleValidations')
            ->add('childrenAdvancedValidations' , null , [
                'label' => 'Advanced validations' ,
            ])
            ->add('displayCode')
            ->add('displayReason')
            ->add('displayMessage');
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\AdvancedValidation' ,
            'category'   => null ,
        ]);
    }
}
