<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\QuestionCategory;
use AppBundle\Entity\Report;
use AppBundle\Services\SimpleValidationService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SimpleValidationType
 *
 * @package AppBundle\Form
 */
class SimpleValidationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder , array $options)
    {
        /** @var Category $category */
        $category = $options['category'];
        $sheets = [];
        $lists = [];
        /** @var Report $report */
        foreach ($category->getReports() as $report) {
            $sheets[$report->getSheetName()] = $report->getSheetName();
            $questionCategoryGroups = $report->getQuestionCategories();
            /** @var QuestionCategory $group */
            foreach ($questionCategoryGroups as $group) {
                if ($group->getUseAsList()) {
                    $lists['#' . $group->getId() .' '. $group->getName()] = $group->getId();
                }
            }
        }

        $typeOptionsSource = SimpleValidationService::getConstants();
        $typeOptions = [];
        foreach ($typeOptionsSource as $value) {
            $typeOptions[str_replace('_' , ' ' , $value)] = $value;
        }

        $builder
            ->add('name')
            ->add('description')
            ->add('autoRun' , CheckboxType::class , [
                'label'    => 'Auto RUN' ,
                'required' => false ,
            ])
            ->add('sourceField' , null , [
                'attr'     => [
                    'placeholder' => 'A3' ,
                ] ,
                'required' => false ,
            ])
            ->add('sourceList' , ChoiceType::class , [
                'choices'     => $lists ,
                'placeholder' => '--' ,
                'required'    => false ,
            ])
            ->add('sourceSheet' , ChoiceType::class , [
                'choices'     => $sheets ,
                'placeholder' => '--' ,
                'required'    => false ,
            ])
            ->add('type' , ChoiceType::class , [
                'choices'     => $typeOptions ,
                'placeholder' => '--' ,
            ])
            ->add('compareTo' , null , [
                'attr'     => [
                    'placeholder' => 'A3' ,
                ] ,
                'required' => false ,
            ])
            ->add('compareToSheet' , ChoiceType::class , [
                'choices'     => $sheets ,
                'placeholder' => '--' ,
                'required'    => false ,
            ])
            ->add('displayCode')
            ->add('displayReason')
            ->add('displayMessage');
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'AppBundle\Entity\SimpleValidation' ,
            'category'   => null ,
        ]);
    }
}
