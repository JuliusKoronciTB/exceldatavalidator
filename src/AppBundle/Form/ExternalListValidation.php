<?php

namespace AppBundle\Form;

use AppBundle\Entity\Category;
use AppBundle\Entity\QuestionCategory;
use AppBundle\Entity\Report;
use AppBundle\Services\ExternalListValidationService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ExternalListValidation
 *
 * @package AppBundle\Form
 */
class ExternalListValidation extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Category $category */
        $category = $options['category'];
        $externalLists = $options['externalLists'];
        $internalLists = [];
        /** @var Report $report */
        foreach ($category->getReports() as $report) {
            $sheets[$report->getSheetName()] = $report->getSheetName();
            $questionCategoryGroups = $report->getQuestionCategories();
            /** @var QuestionCategory $group */
            foreach ($questionCategoryGroups as $group) {
                if ($group->getUseAsList()) {
                    $internalLists['#' . $group->getId() . ' ' . $group->getName()] = $group->getId();
                }
            }
        }

        $typeOptionsSource = ExternalListValidationService::getConstants();
        $typeOptions = [];
        foreach ($typeOptionsSource as $value) {
            $typeOptions[str_replace('_', ' ', $value)] = $value;
        }

        $builder
            ->add('name')
            ->add('description')
            ->add('autoRun', CheckboxType::class, [
                'label'    => 'Auto RUN',
                'required' => false,
            ])
            ->add('sourceField', null, [
                'attr'     => [
                    'placeholder' => 'A',
                ],
                'required' => false,
                'label'    => 'Source Column',
            ])
            ->add('sourceField2', null, [
                'attr'     => [
                    'placeholder' => 'B',
                ],
                'required' => false,
                'label'    => 'Source Column - second condition',
            ])
            ->add('sourceList', ChoiceType::class, [
                'choices'     => $internalLists,
                'placeholder' => '--',
                'required'    => false,
            ])
            ->add('type', ChoiceType::class, [
                'choices'     => $typeOptions,
                'placeholder' => '--',
            ])
            ->add('compareTo', null, [
                'attr'     => [
                    'placeholder' => 'A',
                ],
                'required' => false,
                'label'    => 'Compare To Column',
            ])
            ->add('compareTo2', null, [
                'attr'     => [
                    'placeholder' => 'B',
                ],
                'required' => false,
                'label'    => 'Compare To Column - second condition',
            ])
            ->add('externalList', ChoiceType::class, [
                'choices'     => $externalLists,
                'placeholder' => '--',
                'required'    => false,
                'mapped'      => false,
            ])
            ->add('displayCode', null, [
                'label' => 'Pass Message',
            ])
            ->add('displayReason', null, [
                'label' => 'Error Message',
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @throws \Symfony\Component\OptionsResolver\Exception\AccessException
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'    => 'AppBundle\Entity\SimpleValidation',
            'category'      => null,
            'externalLists' => null,
        ]);
    }
}
